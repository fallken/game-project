<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('verify/{token}', 'Api\AuthController@verifyEmail')->name('verifyEmail');
//PASSWORD RESTE
//Route::get('password-reset', 'Api\PasswordController@showForm'); //I did not create this controller. it simply displays a view with a form to take the email
Route::post('auth/forgot','Api\PasswordController@forgot');
Route::post('auth/do_reset','Api\PasswordController@doReset');

//LOGIN USING SOCIALITE
Route::get('social/{provider}', 'Api\SocialController@redirect');
Route::get('social/{provider}/callback','Api\SocialController@Callback');



Route::group(['middleware' => ['json.response']], function () {

    // public routes
    Route::post('/login', 'Api\AuthController@login')->name('login.api');
    Route::post('/register', 'Api\AuthController@register')->name('register.api');

    Route::post('/increaseCredit','Api\MatchController@increaseCredit');
    Route::post('/decreaseCredit','Api\MatchController@descreaseCredit');

    Route::get('/games/{category_id?}','Api\GameController@index');
    // private routes
    Route::middleware('auth.api')->group(function () {
        #routes which need user to be active 
        Route::group(['middleware'=>'checkAuthUser:hasGame'],function(){
            Route::post('createMatch','Api\MatchController@createMatch');
            Route::post('editMatch','Api\MatchController@editMatch');
            Route::delete('leaveMatch','Api\MatchController@deleteMatch');
            Route::post('acceptMatchInvitiation','Api\MatchController@joinMatchByInvitations');
            Route::post('joinMatch','Api\MatchController@joinMatchAsSideAdmin');
            Route::post('cancellMatch','Api\MatchController@cancellMatch');
            Route::post('inviteUserToMatch','Api\MatchController@inviteUserToMatch');
            Route::post('leaveMatch','Api\MatchController@userLeaveMatch');
            Route::post('getUserMatches','Api\MatchController@getUserMatches');
            Route::post('kickMatchPlayer','Api\MatchController@kickMatchUser');
            Route::post('kickMatchSide','Api\MatchController@kickMatchSide');
            Route::get('userInvitations','Api\MatchController@getUserInvitations');
            Route::get('getUserInvitations','Api\MatchController@getUserInvitations');
            #this function is a bit controversial so we cant make desisions right now . 
            Route::post('kickAdmin','Api\MatchController@kickAdmin');
            Route::post('joinMatchInvitation','Api\MatchController@joinMatchByInvitations');
            #letting the user to register as game admin
            Route::post('requestForGameAdmin','Api\GameAdminController@requestForGameAdmin');

        });
        #routes which are for the game admin 
        Route::group(['middleware'=>'checkAuthUser:isGameAdmin'],function(){
            Route::post('adminJoinMatch','Api\GameAdminController@adminJoinMatch');
            Route::post('adminLeaveMatch','Api\GameAdminController@adminLeaveMatch');
            Route::post('adminStartMatch','Api\GameAdminController@adminStartMatch');
            Route::post('adminEndMatch','Api\GameAdminController@endMatch');
            Route::post('adminPauseMatch','Api\GameAdminController@pauseMatch');
            Route::post('adminChangeMatchStatus','Api\GameAdminController@changeMatchStatus');
            Route::post('kickMatchUser','Api\GameAdminController@kickMatchUser');
        });
        #public routes for basic usser (its still user and means user should be authenticated)
            Route::get('user', 'Api\AuthController@user');
            Route::post('matches','Api\MatchController@matches');
            Route::get('test_match','Api\MatchController@testMatches');
            Route::post('getMatch','Api\MatchController@getMatch');
            Route::post('addFriend','Api\UserController@addFriend');
            Route::post('updateFriendRequest','Api\UserController@setFriendReuqestStatus');
            Route::post('searchUsers','Api\UserController@searchUsers');
            Route::get('getUserProfile','Api\UserController@getUserProfile');
            Route::get('friendRequests','Api\UserController@friendRequests');
            Route::post('addGameAccount','Api\MatchController@addGameAccount');
            Route::get('categories', 'Api\CategoryController@index');
            Route::get('categories/{categoryId}', 'Api\CategoryController@show');
            Route::post('categories', 'Api\CategoryController@storeOrUpdate');
            Route::post('send_invitation','Api\MatchInvitationController@sendMatchInvitation');
            Route::get('/logout', 'Api\AuthController@logout')->name('logout');
            #user messaging
            Route::post('/sendMessage','Api\UserController@sendMessageToUser');
            Route::get('/userMessages','Api\UserController@userMessages');
    });

});
