<?php

namespace Tests\Unit;

use App\Models\GameFile;
use App\Repositories\GameFilesRepository;

use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class FileUploadTest extends TestCase
{
    /**
     * @var GameFilesRepository
     */
    private $gameFileRepository;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->gameFileRepository = new GameFilesRepository();
    }

    /**
     * Image upload test.
     *
     * @return void
     */
    public function testImageUpload()
    {
        Storage::fake('public');

        $result = $this->gameFileRepository->upload(UploadedFile::fake()->image('photo1.jpg'), GameFile::TYPE_IMAGE);

        print_r($result);

        $this->assertIsObject($result);
    }

    /**
     * Video upload test.
     *
     * @return void
     */
    public function testVideoUpload()
    {
        Storage::fake('public');

        $result = $this->gameFileRepository->upload(UploadedFile::fake()->create('tests.mp4', 145000), GameFile::TYPE_VIDEO);

        print_r($result);

        $this->assertIsObject($result);
    }

    /**
     * Other upload test.
     *
     * @return void
     */
    public function testFileUpload()
    {
        Storage::fake('public');

        $result = $this->gameFileRepository->upload(UploadedFile::fake()->create('tests.pdf', 3780), GameFile::TYPE_OTHER);

        print_r($result);

        $this->assertIsObject($result);
    }

    /**
     * Get uploaded file url link test.
     *
     * @return void
     */
    public function testGetUploadedFileUrlLink()
    {
        Storage::fake('public');

        $gameFile = GameFile::find(15);

        $result = $this->gameFileRepository->asset((object)$gameFile->toArray());

        print_r($result);

        $this->assertIsString($result);
    }
}
