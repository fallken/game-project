<?php

namespace Tests\Unit;

use App\Repositories\GameFilesRepository;
use App\Repositories\CategoriesRepository;

use Illuminate\Support\Str;
use Tests\TestCase;

class CategoriesTest extends TestCase
{
    /**
     * @var CategoriesRepository
     */
    private $categoriesRepository;

    /**
     * @var GameFilesRepository
     */
    private $gameFilesRepository;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->categoriesRepository = new CategoriesRepository();
        $this->gameFilesRepository = new GameFilesRepository();
    }

    /**
     * Image upload test.
     *
     * @return void
     */
    public function testCreateCategory()
    {
        $category = $this->categoriesRepository->setName(Str::random(16))->save();

        print_r($category->toArray());

        $this->assertIsArray($category->toArray());
    }

    /**
     * Image upload test.
     *
     * @return void
     */
    public function testUpdateCategoryWithImage()
    {
        $image = $this->gameFilesRepository->fetchById(1);

        $category = $this->categoriesRepository
            ->setName(Str::random(16))
            ->setImage($image->name, $image->id)
            ->save(1);

        print_r($category->toArray());

        $this->assertIsArray($category->toArray());
    }

    /**
     * Remove category test.
     */
    public function testRemoveCategory()
    {
        $this->categoriesRepository->remove(1);
        $this->categoriesRepository->remove(2, true);

        $this->assertNull($this->categoriesRepository->fetchById(1));
        $this->assertNull($this->categoriesRepository->fetchById(2));
    }
}
