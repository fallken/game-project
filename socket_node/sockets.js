const socketiojwt = require('socketio-jwt');
require("dotenv").config({path: '../../.env'});
const redisClient = require('./bin/redis');
// enable debugging
var Redis = require('ioredis');
const redis = require('socket.io-redis');
var newMsg = new Redis(
    process.env.REDIS_PORT ? process.env.REDIS_PORT : '8090'
    , process.env.REDIS_HOST ? process.env.REDIS_HOST : '127.0.0.1'
);
newMsg.psubscribe('*', function (err, count) {
    console.log('Subscribed');
});
var sockets = {};
var users = {};
var friends = {};
var lockedUserFriends = {};
var systemNotifs = ['new_friend', 'removed_friend', 'unlockUserFriend'];
var socketObj;//will set the socket obj on it so that we could send it to all the users .
sockets.init = function (server) {
    var io = require('socket.io').listen(server);
    //using redis for clustering socket io instances
    io.adapter(redis({
        host: process.env.REDIS_HOST ? process.env.REDIS_HOST : '127.0.0.1',
        port: process.env.REDIS_PORT ? process.env.REDIS_PORT : '8090'
    }));
    io.sockets
        .on('connection', socketiojwt.authorize({
            secret: process.env.JWT_SECRET,
            timeout: 15000 // 15 seconds to send the authentication message
        })).on('authenticated',async function (socket) {
        //this socket is authenticated, we are good to handle more events from it.
        socketObj = socket;
        console.log('decoded token');
        console.log(socket.decoded_token);
        const user_id = socket.decoded_token.user_id;
        const user_friends = socket.decoded_token.friends ? socket.decoded_token.friends : [];
        const socket_id = socket.id;
        await addUserId(user_id, socket_id, user_friends);
        let friends = await getFriends();
        let users = await getUsers();
        if (friends[user_id]) {
            let onlineFriends = [];
            for (let i = 0; i < friends[user_id].length; i++) {//if the user has any friends
                if (users[friends[user_id][i]]) {//if the friends of the user are online
                    onlineFriends.push(friends[user_id][i]);
                }
            }
            if (onlineFriends.length > 0) {
                await pushToSockets(user_id, 'online_friends', {
                    message: onlineFriends,
                });
            }
        }
        socket.on('disconnect',async function () {
            await removeUserSocket(socket.decoded_token.user_id, socket.id);
        });
    });

    newMsg.on('pmessage',async function (subscribed, channel, payload) {
        if(isJson(payload)){
            let parsedData = JSON.parse(payload);
            parsedData = JSON.parse(parsedData.data.message);
            if (systemNotifs.indexOf(parsedData.name) !== -1) {//add the friend list of the user in another object.
                console.log('its working');
                await handleCustomMessages(parsedData.name, parsedData);
            } else {
                if (parsedData.all && parsedData.all === true) {//if the message is meant to be for all
                    // socketObj.broadcast.emit(parsedData.name, parsedData.message);
                    console.log('sending to all');
                    io.sockets.emit(parsedData.name, parsedData.message);
                } else {
                    console.log(`sending to ${parsedData.to}`);
                    //the message is going to be an array which will eventually contain the message text and other things like sender id , room_id , match_id (depending on type of the message).
                    console.log(parsedData);
                    if (parsedData.to.length > 0) {
                        await pushToSockets(parsedData.to, parsedData.name, {
                            message: parsedData.message,
                            details: parsedData.details
                        });
                    }
                }
            }
        }
    });
    const  pushToSockets = async function (userKey, name, data) {//will push result to each active user
        // socket instance on the front side . user Arr should in array of users.
        let users = await getUsers();
        console.log('users are ');
        console.log(users);
        console.log('users are ');
        if (userKey instanceof Array) {
            for (let j = 0; j < userKey.length; j++) {//look for each users log.
                if (users[userKey[j]]) {
                    for (var i = 0; i < users[userKey[j]].length; i++) {
                        console.log('sending it to');
                        console.log(users[userKey[j]][i]);
                        io.to(users[userKey[j]][i]).emit(name, data);
                    }
                }
            }
        } else {
            if (users[userKey]) {
                for (let key = 0; key < users[userKey].length; key++) {
                    console.log('sending it to');
                    console.log(name);
                    console.log(users[userKey][key]);
                    io.to(users[userKey][key]).emit(name, data);
                }
            }
        }
    };

    const addUserId = async (user_id, socket_id, user_friends = []) => {
        let users = await getUsers();
        let friends = await getFriends();
        let lockedFriends = await getLockedFriends();
        if (Array.isArray(users[user_id])) {
            users[user_id].push(socket_id);
            users = await setUsers(users);
        } else {
            users[user_id] = [socket_id];
            users = await setUsers(users);
        }
        if (!lockedFriends[user_id]) {//if the user friends log is set to true done let him to add the friends (cause his friends which are obtained from his token are not valid )
            friends[user_id] = user_friends;
            friends = await setFriends(friends);
        }
        console.log('users are ');
        console.log(users);
        console.log(friends);
    };
    const handleCustomMessages = async (name, parsed_data) => {
        switch (name) {
            case 'new_friend':
                await newFriendHandler(parsed_data);
                break;
            case 'removed_friend':
                await removed_friend(parsed_data);
                break;
            case 'unlockUserFriend':
                await unlockFriendsLock(parsed_data);
                break;
        }
    };
    //lock the user friends payload in the toke so that the users friends are not resetted each time he starts a new session.
    const newFriendHandler = async (parsedData) => {
        let friends =await getFriends();
        let lockedUserFriends =await getLockedFriends()
        //the message is going to be an array which will eventually contain the message text and other things like sender id , room_id , match_id (depending on type of the message).
        console.log(`sending newFriendHandler to ${parsedData.to}`);
        await pushToSockets(parsedData.to, parsedData.name, {
            message: parsedData.message,
            details: parsedData.details
        });
        friends[parsedData.to] = parsedData.details.friends;
        await setFriends(friends);
        lockedUserFriends[parsedData.to] = true;
        await setLockedFriends(lockedUserFriends);
    };
    const removed_friend = async (parsedData) => {
        let friends  = await getFriends();
        let lockedUserFriends = await getLockedFriends();
        console.log(`sending removed_friend to ${parsedData.to}`);
        await pushToSockets(parsedData.to, parsedData.name, {
            message: parsedData.message,
            details: parsedData.details
        });
        console.log('friends sent here are');
        console.log(parsedData.details.friends);

        friends[parsedData.to] = parsedData.details.friends;
        lockedUserFriends[parsedData.to] = true;

        await setFriends(friends);
        await setLockedFriends(lockedUserFriends);

        console.log('locked 2users');
        console.log(lockedUserFriends);
    };
    const unlockFriendsLock = async (parsed_data) => {
        let lockedUserFriends = await getLockedFriends();
        lockedUserFriends[parsed_data.to] = false;
        await setLockedFriends(lockedUserFriends);
        console.log('locked 32users');
        console.log(lockedUserFriends);
    };
    const removeUserSocket =async (user_id, socket_id) => {
        let users = await getUsers();
        if (users[user_id]) {
            users[user_id] = users[user_id].filter(item => item !== socket_id);
            if (users[user_id].length === 0) {
                delete users[user_id];
                //send message to his friends that he is offline
                if (friends[user_id] && friends[user_id].length > 0) {
                    await pushToSockets(friends[user_id], 'friend_offline', {message: user_id, details: []});
                }
            }
        }
        await setUsers(users);
        console.log(users);
        return true;
    };

    const getUsers = async () => {
        let users = await redisClient.getAsync('users');
        if (users)
            return JSON.parse(users);
        else
            return {};
    }
    const setUsers = async (users) => {
        users = JSON.stringify(users);
        await redisClient.set('users',users);
        return await redisClient.getAsync('users');
    }
    const getFriends = async () => {
        let friends = await redisClient.getAsync('friends');
        if (friends)
            return JSON.parse(friends);
        else
            return {};
    }
    const setFriends = async (friends) => {
        friends = JSON.stringify(friends);
        await redisClient.set('friends',friends);
        return await redisClient.getAsync('friends');
    }
    const getLockedFriends = async () => {
        let lockedFriends = await redisClient.getAsync('friends');
        if (lockedFriends)
            return JSON.parse(lockedFriends);
        else
            return {};
    }
    const setLockedFriends = async (lockedFriends) => {
        lockedFriends = JSON.stringify(lockedFriends);
        await redisClient.set('lockedFriends',lockedFriends);
        return await redisClient.getAsync('lockedFriends');
    }
    const isJson = (data) => {
            try {
                JSON.parse(data);
            } catch (e) {
                return false;
            }
            return true;
    }
};

module.exports = sockets;