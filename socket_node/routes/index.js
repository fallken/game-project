var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  console.log('its working');
  res.render('index', { title: 'Express' });
});
router.get('/app/:id?', function(req, res, next) {
  let token;
  if(req.params.id){
    let id = req.params.id;
        if(id == 1){
          token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9nYW1lY291cnQubG9jYWxcL2FwaVwvbG9naW4iLCJpYXQiOjE1NzU5ODUzNzQsImV4cCI6MTYwNzA4OTM3NCwibmJmIjoxNTc1OTg1Mzc0LCJqdGkiOiJreDdqeEtHNjJSdWlxQ25kIiwic3ViIjoxLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3IiwidXNlcl9pZCI6MSwibmFtZSI6IiAgIGZhcmFtYXJ6IiwiZnJpZW5kcyI6WzIsM119.xREUkyRJmWatWZsNdFtl8fn2-k5rWgdaoWtDxrrBz2Q';

        }
        if(id == 2){
          console.log('setting 2')
          token='eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9nYW1lY291cnQubG9jYWxcL2FwaVwvbG9naW4iLCJpYXQiOjE1NzU5Nzc0OTQsImV4cCI6MTYwNzA4MTQ5NCwibmJmIjoxNTc1OTc3NDk0LCJqdGkiOiJDazNndTVxMWtsMzlkYkxuIiwic3ViIjoyLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3IiwidXNlcl9pZCI6MiwibmFtZSI6IkJ1cmxleSBVcHRvbiIsImZyaWVuZHMiOlsxXX0.oNeaRHm2b2FMhTa7lDhurP3uas6PqqOkCOCVxiLNrSk';
        }
        if(id == 3){
          console.log('setting 3')
          token='eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9nYW1lY291cnQubG9jYWxcL2FwaVwvbG9naW4iLCJpYXQiOjE1NzYyNjYzNDQsImV4cCI6MTYwNzM3MDM0NCwibmJmIjoxNTc2MjY2MzQ0LCJqdGkiOiJsdDBoUGl0ZkZNSWtuNjBxIiwic3ViIjozLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3IiwidXNlcl9pZCI6MywibmFtZSI6IkRyLiBBbGljaWEgQmVlciIsImZyaWVuZHMiOlsxXX0.JmXibdU2uB9zkHelHDeqS7ofiFv6yJ5lWjM0fanqPmI';
        }
    }else{//set default for user 1
      token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9nYW1lY291cnQubG9jYWxcL2FwaVwvbG9naW4iLCJpYXQiOjE1NzU5ODIyMjYsImV4cCI6MTYwNzA4NjIyNiwibmJmIjoxNTc1OTgyMjI2LCJqdGkiOiJJdmxqVkJsU1JZNzc0dEtKIiwic3ViIjozLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3IiwidXNlcl9pZCI6MywibmFtZSI6IkRyLiBBbGljaWEgQmVlciIsImZyaWVuZHMiOltdfQ.6ErI0HFs2C92A4pv6Twltd5N9nzUxSDw-2VcjQZ23W8'
    }

  if(req.query.token){
    token=req.query.token;
  }
  console.log(`${'the port is '}`);
  console.log(process.env.port);

  res.render('socket.ejs',{token:token,port:process.env.port});
});

module.exports = router;
