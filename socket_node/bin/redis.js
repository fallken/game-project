require("dotenv").config({path: '../../.env'});
const redis = require('redis');
const bluebird = require('bluebird');//for promisifying the redis functionalities.
// const {promisify} = require('util');
//
// let options = !process.env.LOCAL ? {
//     password: settings.redis_pass
// } : {};

const redis_port = process.env.REDIS_PORT ? process.env.REDIS_PORT : '6379';
const redis_host = process.env.REDIS_HOST ? process.env.REDIS_HOST : '127.0.0.1';

bluebird.promisifyAll(redis);
const redClient = redis.createClient(redis_port,redis_host);

module.exports = redClient;