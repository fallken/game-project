/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var Redis = require('ioredis');
var onlineIdChannel = new Redis();
var newFriendsChannel = new Redis();
var vote = new Redis();
var newMsg = new Redis();
var playerStatus = new Redis();
var adminJoin = new Redis();

onlineIdChannel.subscribe('onlineIds');
newMsg.subscribe('messages');
newFriendsChannel.subscribe('newFriends');
playerStatus.subscribe('playerStatus');
adminJoin.subscribe('adminJoin');//listens for admin joining or leaving the queue
vote.subscribe('vote');


var arr = [];
var testArr = [];
var key = 0;
io.on('connection', function (socket) {//adding each user to local arr for using late on
    socket.on('ids', function (data) {
        addUserToArray(socket.id, data);
        arr = arr.filter(function (n) {
            return n != undefined
        });//filtering white spaces in array left from deletion of elements
        console.log('connected')
        console.log(arr)
        // console.log(socket.id)
        // console.log(data)
        io.to(socket.id).emit('online', onlinefriends(data.userId));//should create a function for emitting for each specific sucket id
        setMeOnline(data.userId)
    })
    socket.on('disconnect', function () {
        var user = getUserBySocketId(socket.id)
        if (user !== undefined) {
            console.log('socket Id: ');
            console.log(socket.id)
            //will possibly send call for active pages to see if there is still another page existing around or not
            io.to(socket.id).emit('AreYouOnline', 'are you online?');
            console.log('user:');
            console.log(user)
            setOffLine(user);
            deleteUserFromArray(socket.id);
            console.log('disconnected');
            console.log('final array:')
            console.log(arr)
            arr = arr.filter(function (n) {
                return n != undefined
            });
        }
    });
});
//message handling section for handling new incoming messages
newMsg.on('message', function (channel, message) {
    msg = JSON.parse(message)
    for (var i = 0; i < arr.length; i++) {
        if (arr[i][1] == msg.user_target) {
            // io.to(arr[i][0]).emit('message', msg);
            pushToSockets(arr[i],'message',msg);
            console.log('the ' + arr[i][1] + 'is matching the information')
        }
    }
    console.log(msg);
});
//listening for new friends and their friend to add them as online ones
newFriendsChannel.on('message', function (channel, ids) {
    data = JSON.parse(ids)//parsing the ids which contain two keys : one user id and the other one target id
    for (var i = 0; i < arr.length; i++) {
        if (arr[i][1] == data.user_id) {//sending new friend requesting user
            arr[i][2].push(data.target_id)
            // io.to(arr[i][0]).emit('newFriend', data.target_info);
            pushToSockets(arr[i],'newFriend',data.target_info)
            // io.to(arr[i][0]).emit('online', data.target_id);
            pushToSockets(arr[i],'online',data.target_id)
            console.log('the ' + arr[i][1] + 'is matching the information')
        }
        if (arr[i][1] == data.target_id) {//sending new friend information to target user
            arr[i][2].push(data.user_id)
            // io.to(arr[i][0]).emit('newFriend', data.user_info);
            pushToSockets(arr[i],'newFriend',data.user_info)
            // io.to(arr[i][0]).emit('online', data.user_id);
            pushToSockets(arr[i],'online',data.user_id)
            console.log('sending requesting user information to target user')
        }
    }
});
//listening for online user ids
onlineIdChannel.on('message', function (channel, friendId) {
    searchForFriends(arr, friendId);
});
//listening on joiners or leavers in the queue
playerStatus.on('message',function (channel,user) {
    data = JSON.parse(user)//parsing user which was turned into json data in the begining
    if (data.status == 0){//if the player is leaving
        //todo:turn looping throgh user info arr(arr array) into a seperate local function to go according to DRY methodology
        for (var i = 0; i < arr.length; i++) {
            loop2: for (var j = 0; j < arr[i][3].length; j++) {
                if (arr[i][3][j] == data.queueId) {
                    pushToSockets(arr[i], 'left', [data.name,data.id,data.queueId]);
                    break loop2;
                }
            }
        }
    } else if(data.status == 1){//if the player is joining the queue
        for (var i = 0; i < arr.length; i++) {
            loop2: for (var j = 0; j < arr[i][3].length; j++) {
                if (arr[i][3][j] == data.queueId) {
                    pushToSockets(arr[i], 'joined', [data.name,data.id,data.queueId]);
                    break loop2;
                }
            }
        }
    }
})
//listening for incoming voting requests(this function will be a bit complicated cause it will look for the targetId
vote.on('message',function(channel,voteReq){
    data = JSON.parse(voteReq)
    console.log('the target is :' + data.target)
    if (data.target == 1){
        console.log('going to push to sockets right now')
        for (var i = 0; i < arr.length; i++) {
            loop2: for (var j = 0; j < arr[i][3].length; j++) {
                if (arr[i][3][j] == data.queueId) {
                    console.log('this user will be notified '+arr[i]);
                    pushToSockets(arr[i], 'voteAdminKick', [data]);
                    break loop2;
                }
            }
        }
    }
})
//listening for admin joining the queue or leaving it
adminJoin.on('message',function(channel,admin){
    data = JSON.parse(admin)
    console.log('status is :' + data.status);
    console.log('queueId is :' + data.queueId);
    if (data.status == 0){//if admin leaving queue
        for (var i = 0; i < arr.length; i++) {
            loop2: for (var j = 0; j < arr[i][3].length; j++) {
                if (arr[i][3][j] == data.queueId) {
                    console.log('this user will be notified '+arr[i]);
                    pushToSockets(arr[i], 'adminLeft', [data.name,data.id,data.queueId]);
                    break loop2;
                }
            }
        }
    } else if(data.status == 1){//if admin joining queue
        console.log('admin has joined queue '+ data.admin)
        for (var i = 0; i < arr.length; i++) {
            loop2: for (var j = 0; j < arr[i][3].length; j++) {
                if (arr[i][3][j] == data.queueId) {
                    console.log('this user will be notified '+arr[i]);
                    pushToSockets(arr[i], 'adminJoin', [data.name,data.id,data.queueId]);
                    break loop2;
                }
            }
        }
    }
})
//starting node server on port 3000
http.listen(3000, function () {
    console.log('Listening on Port 3000');
});
//local function for finding the online users with the specfied ids
var searchForFriends = function (arr, id) {
    for (var i = 0; i < arr.length; i++) {
        for (var j = 0; j < arr[i][2].length; j++) {
            if (arr[i][2][j] == id) {
                console.log('the id of ' + arr[i][1] + ' will be notified');
                // io.to(arr[i][0]).emit('online', id);
                pushToSockets(arr[i],'online',id)
            }
        }
    }
}
var deleteUserFromArray = function (socketId) {
    for (var i = 0; i < arr.length; i++) {
        for (var j = 0; j < arr[i][0].length; j++) {
            if (arr[i][0][j] == socketId) {
                if (arr[i][0].length == 1) {
                    arr.splice(i, 1);//deleting the whole array when there will be no sckets left after deleting
                    return true //breaking out of the function
                }
                arr[i][0].splice(j, 1)//deleting the corresponding socket id
            }
        }
    }
}
var addUserToArray = function (socketId, data) {
    for (var i = 0; i < arr.length; i++) {
        if (arr[i][1] == data.userId) {
            arr[i][0].push(socketId)
            return true;
        }
    }//if its not in the array so we will add it
    data.queueIds=data.queueIds == undefined ? [] :data.queueIds ;
    arr[key] = [[socketId], data.userId, data.friends,data.queueIds];
    key++;
}
var onlinefriends = function (userId) {
    var onlineFriends = [];
    for (var i = 0; i < arr.length; i++) {
        for (var j = 0; j < arr[i][2].length; j++) {
            if (arr[i][2][j] == userId) {
                onlineFriends.push(arr[i][1])
            }
        }
    }
    return onlineFriends;
}
let getUserBySocketId = function (socketId) {
    for (var i = 0; i < arr.length; i++) {
        for (var j = 0; j < arr[i][0].length; j++) {
            if (arr[i][0][j] == socketId) {
                return arr[i]
            }
        }
    }
}
let setOffLine = function (user) {
    for (var i = 0; i < arr.length; i++) {
        loop2: for (var j = 0; j < arr[i][2].length; j++) {
            if (arr[i][2][j] == user[1]) {
                // io.to(arr[i][0]).emit('offline',userId);
                if (user[0].length == 1){
                    pushToSockets(arr[i], 'offline', user[1]);
                    break loop2;
                }
            }
        }
    }
};
let setMeOnline = function (user) {
    for (var i = 0; i < arr.length; i++) {
        loop2: for (var j = 0; j < arr[i][2].length; j++) {
            if (arr[i][2][j] == user) {
                pushToSockets(arr[i], 'online', user);
                break loop2;
            }
        }
    }
};
let pushToSockets = function (userArr, name, data) {//will push result to each active user socket instance on the front side
    for (var i = 0; i < userArr[0].length; i++) {
        io.to(userArr[0][i]).emit(name, data);
    }
}
