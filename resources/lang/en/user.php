<?php


return [
    'cant_add_friend_himself'=>'user cannot add himself as friend',
    'user_already_friend'=>'the user is already in your friend list',
    'friend_request_exists'=>'the friend already exists and is unanswered',
    'problem_sending_request'=>'there was a problem sending the friend request ',
    'friend_request_success'=>'sent friend request successfully',
    'friend_request_not_exist'=>'the friend request is not available',
    'user_not_own_request'=>'the request is not for the user',
    'maximum_friend_limit'=>'the user has reached the limit of maximum friends ',
    'request_unavailable'=>'the request is no longer available',
    'problem_changin_friend_request_status'=>'there was a problem changing the request status',
    'request_status_change_success'=>'request status changed successfully'
];