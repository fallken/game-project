<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTournametsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tournaments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->integer('creator_id');//will actaully be the main admin ad who created the game .
            $table->timestamp('start_time');
            $table->integer('team_allowed_members');
            $table->integer('teams');
            $table->text('description')->nullable();
            $table->string('banner')->nullable();
            $table->unsignedBigInteger('game_id');
            $table->decimal('entrance_price',15,2)->default(0.00);
            $table->decimal('first_winner_prize',15,2)->default(0.00);
            $table->decimal('second_winner_prize',15,2)->default(0.00);
            $table->decimal('third_winner_prize',15,2)->default(0.00);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tournaments');
    }
}
