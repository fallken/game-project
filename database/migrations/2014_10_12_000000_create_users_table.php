<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique()->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->string('mobile')->nullable();
            $table->string('mobile_verified_at')->nullable();
            $table->enum('currency', config('currency'))->default('IRR')->comment('ISO 4217');
            $table->integer('status')->default(0);
            $table->integer('max_friend_no')->default(50);
            $table->string('email_verification_token',35)->nullable();
            $table->string('avatar',150)->nullable();
            $table->string('provider')->nullable();//for using on o'auth client
            $table->string('provider_id')->nullable();
            $table->decimal('wallet', 15, 2)->default(0.00);
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
