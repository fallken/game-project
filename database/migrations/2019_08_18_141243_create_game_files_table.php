<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGameFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_files', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 128);
            $table->bigInteger('size')->unsigned();
            $table->bigInteger('creator_id')->unsigned();
            $table->enum('type', ['image', 'video', 'other']);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('game_files');
    }
}
