<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('game_id')->unsigned();
            $table->bigInteger('creator_id')->unsigned();
            $table->bigInteger('host_user_id')->unsigned();
            $table->bigInteger('guest_user_id')->unsigned()->nullable();
            $table->unsignedBigInteger('host_team_id')->nullable();
            $table->unsignedBigInteger('guest_team_id')->nullable();
            $table->unsignedBigInteger('tournament_id')->nullable();//if there is a tournament id then things will change for sure .
            $table->integer('joined_users')->default(1);
            $table->timestamp('start_time')->nullable();
            $table->decimal('price',15,2)->default(0.00);
            $table->enum('currency', config('currency'))->default('IRR')->comment('ISO 4217');
            $table->enum('status',config('matches.status'))->default('pending');
            $table->integer('max_players')->nullable();
            $table->integer('min_players')->nullable();
            $table->integer('host_side_players')->nullable();
            $table->integer('guest_side_players')->nullable();
            $table->bigInteger('admin_id')->unsigned()->nullable();
            $table->string('message')->nullable();
            $table->integer('min_level_allowed')->nullable();
            $table->integer('max_level_allowed')->nullable();
            $table->enum('winner_side', ['host', 'guest'])->nullable();
            $table->enum('host_pay', ['single', 'all'])->default('single');
            $table->enum('guest_pay', ['single', 'all'])->default('single');
            $table->timestamps( );

            $table->foreign('creator_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matches');
    }
}
