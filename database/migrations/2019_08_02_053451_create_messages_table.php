<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->bigInteger('user_name')->unsigned()->nullable();
            $table->bigInteger('match_id')->unsigned()->nullable();
            $table->bigInteger('match_side_id')->nullable();
            $table->bigInteger('admin_id')->nullable();
            $table->string('admin_name')->nullable();
            $table->bigInteger('super_admin_id')->nullable();
            $table->bigInteger('target_user_id')->unsigned()->nullable();
            $table->string('text')->nullable();
            $table->enum('type', ['system','talk','notification','admin_message','system_notification'])->default('system');
            $table->timestamp('seen_at')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
