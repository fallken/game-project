<?php

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{


   
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories =[
            ['name'=>'action','image'=>'https://cdn.mos.cms.futurecdn.net/wHXa4AmCcVUcbxTn7k5uoc-970-80.jpg'],
            ['name'=>'MMORPG','image'=>'http://3.bp.blogspot.com/-8pqOIqoyTqo/UdzW_Mf3NFI/AAAAAAAAG9A/GrB0Q6_UuSQ/s400/dota2launch2.jpg'],
            ['name'=>'mobile','image'=>'https://miro.medium.com/max/2880/1*m7eS70d2Nhtk4-3YdHp3mg.jpeg']
        ];

        foreach($categories as $category){
            Category::create([
                'name'=>$category['name'],
                'image'=>$category['image'],
                'creator_id'=>1 #my main id of the user . :)
            ]);
        }   
    }
}
