<?php

use App\Models\Game;
use App\Models\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use Illuminate\Database\Seeder;

class UserGamesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        $users = User::all();
        $games = Game::all();



        $users->each(function($user,$key) use ($faker, $games){
             $games=$games->shuffle();
             $games=$games->slice(3);
             $games->each(function($item,$key) use ($user,$faker){
                    $result = $user->games()->attach($item->id, [
                        'account_user_name' => $faker->userName,
                        'account_id' => Str::random(),
                        'account_level' => rand(1, 100),
                    ]);
             });
        });
             
    }
}
