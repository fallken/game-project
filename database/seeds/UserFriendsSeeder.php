<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class UserFriendsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();
        $userQuery = new User();

        $users->each(function($user) use ($userQuery){
            $userMatches = $user->matches;
            $friends = $userQuery->where('users.id','!=',$user->id)->get()->shuffle()->slice(12);

            $friends->each(function($friend) use($user,$userMatches){
                $friend_matches = collect($friend->matches?Arr::pluck($friend->matches,'id'):null);
                $userMatches = collect($userMatches?Arr::pluck($userMatches,'id'):null);

                if($friend_matches)://if the future friend has any sort of matches ? 
                    $friend_matches->map(function($value) use ($userMatches,$user,$friend){
                        if(array_has($userMatches,$value) === true){
                            $userSide = $user->matches->where('id', $value)->first();
                            $friendSide = $friend->matches->where('id', $value)->first();

                            $userSide = is_null($userSide)?null:$userSide->pivot->side;
                            $friendSide = is_null($friendSide)?null:$friendSide->pivot->side;
                            
                            if(!is_null($userSide) && !is_null($friendSide) && $userSide != $friendSide )://if the user and its friend are not on the opposide sides  of a match then add tem as friends 
//                                $user->friends()->attach($friend->id);
                                return true;//break out of  the map helper function
                            endif;
                        }
                    });
                else:
//                    $user->friends()->attach($friend->id);
                endif;


            });

        }); 
    }
}
