<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create(['name' => 'Supervisor']);
        Role::create(['name' => 'Referee']);
        Role::create(['name' => 'Player']);

        Permission::create(['name' => 'Create game']);
        Permission::create(['name' => 'Create match']);
        Permission::create(['name' => 'Approve referee']);
        Permission::create(['name' => 'Block user']);
        Permission::create(['name' => 'Manage categories']);
    }
}
