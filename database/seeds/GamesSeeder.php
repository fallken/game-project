<?php

use Illuminate\Database\Seeder;

class GamesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $games =[
            ['name_en'=>'clash of clans','category'=>3,'name_fa'=>'کلش اف کلنز','image'=>'https://miro.medium.com/max/2880/1*m7eS70d2Nhtk4-3YdHp3mg.jpeg','status'=>'active'],
            ['name_en'=>'counter strike GO','category'=>1,'name_fa'=>'کانتر استرایک','image'=>'https://cdn-cf.gamivo.com/image_cover.jpg?f=26073&n=47263531930005853.jpg&h=8104eb1555ac18f00b0f464b3a565cae','status'=>'active'],
            ['name_en'=>'call of duty 4','category'=>1,'name_fa'=>'ندای وظیفه ','image'=>'https://hb.imgix.net/e78d4b51e4e5cb9aeeaed4647d528c929c30ed60.jpg?auto=compress,format&fit=crop&h=353&w=616&s=00e07b5b9773deb6fe225f390c73ded5','status'=>'active'],
            ['name_en'=>'Dota 2','category'=>2,'name_fa'=>'دوتا 2','image'=>'https://steamcdn-a.akamaihd.net/steam/apps/570/header.jpg?t=1568136439','status'=>'active'],
        ];
        foreach($games as $game){
            $create_result = App\Models\Game::create([
                'name_en'=>$game['name_en'],
                'name_fa'=>$game['name_fa'],
                'image'=>$game['image'],
                'status'=>$game['status'],
            ]);
            $create_result->categories()->attach($game['category']);
        }
    }
}
