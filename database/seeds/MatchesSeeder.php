<?php

use App\Models\Match;
use App\Models\User;
use Illuminate\Database\Seeder;

class MatchesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $user_ids = collect(range(1,20));

       $prices = collect([10000,15000,40000,200000,100000]);
       $status = collect(['pending', 'ongoing', 'finished', 'stopped', 'failed']);
       $players = collect([4, 8, 12, 6, 15]);
       $user_ids = $user_ids->shuffle();

       $user_ids = $user_ids->slice(0,15);

       $users = User::whereIn('id',$user_ids);

       $users->each(function($user,$index) use($prices,$status, $players){
            $userGames = $user->games->first(); //getting the games of the user 
            $matchData = [
                'game_id' => $userGames->id,
                'creator_id'=> $user->id,
                'host_user_id'=> $user->id,
                'max_players' => $players->shuffle()->first(),
                'price'=> $prices->shuffle()->first(),
                // 'status'=>$status->shuffle()->first(),
            ];
            Match::create($matchData);
       });

    }
}
