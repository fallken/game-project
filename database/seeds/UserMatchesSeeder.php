<?php

use App\Models\Match;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserMatchesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $matches = Match::all();
       $userQuery = new User();
       
       $matches->each(function($match) use ($userQuery){
        //    dd($match->host_user_id);
            $users = $userQuery->where('users.id','!=',$match->host_user_id)->whereHas('games',function($query) use ($match){
                $query->where('games.id',$match->game_id);
            })->with(['games'=>function($query) use ($match){
                $query->where('games.id',$match->game_id);
            }])->get()->shuffle();
            if($match->max_players):
                $users =  $users->slice(0,$match->max_players- 1);//-1 is for the default value of it 
            endif;
            $users->each(function($user,$index) use ($match,$users){
                if($index >= $users->count()/2):
                    $side = 1;//set the side using calculations
                else:
                    $side = 0;
                endif;

                $user->matches()->attach($match->id,['side'=> $side,'user_account_id'=>$user->games->first()->id]);

            });

            $match->update(['joined_users' => $users->count()]);

       });
    }
}
