<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'=>'   faramarz', 'status'=>1 ,'email'=>'arsalani@outlook.com', 'mobile'=>'09198474535', 'password'=>bcrypt('faramarz1374'),'wallet'=>10000
        ]);
        $this->call(UserTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(GamesSeeder::class);
        $this->call(UserGamesSeeder::class);
        $this->call(MatchesSeeder::class);
        $this->call(UserMatchesSeeder::class);
        $this->call(UserFriendsSeeder::class);
        //add a default user for logging in and using the swagger doc
      
        //SETUP LARAVEL PASSPORT INSIT
        Artisan::call('passport:install');
    }
}
