<?php

namespace App\Repositories;

use App\Models\GameFile;
use App\Exceptions\BadUploadFileException;
use App\Interfaces\GameFilesInterface;

use Exception;
use Illuminate\Support\Str;
use Illuminate\Http\UploadedFile;

class GameFilesRepository implements GameFilesInterface
{
    /**
     * Fetch game file from data storage.
     *
     * @param $id
     *
     * @return \App\Models\GameFile|null
     */
    public function fetchById($id)
    {
        return  GameFile::find($id);
    }

    /**
     * Upload and save file info to data storage.
     *
     * @param UploadedFile $file
     * @param string $type
     *
     * @return mixed
     * @throws BadUploadFileException
     */
    public function upload(UploadedFile $file, string $type)
    {
        try {
            $name = time() . Str::random(45);
            $name .= substr($file->getClientOriginalName(), strrpos($file->getClientOriginalName(), '.'));
            $file->storePubliclyAs(config('game-files.path') . $type . '/', $name);
        }
        catch (Exception $exception) {
            throw new BadUploadFileException($exception->getMessage(), $exception->getCode());
        }

        $gameFile = new GameFile(['name' => $name, 'type' => $type, 'size' => $file->getSize()]);

        $gameFile->save();

        return (object)['id' => $gameFile->getAttribute('id'), 'name' => $name, 'type' => $type, 'status' => 'success'];
    }

    /**
     * Upload many files.
     *
     * @param array $files
     * @param string $type
     *
     * @return array
     * @throws BadUploadFileException
     */
    public function uploadMany(array $files, string $type): array
    {
        $results = [];

        foreach ($files as $file) {
            $results[] = $this->upload($file, $type);
        }

        return $results;
    }

    /**
     * Remove game file form data storage.
     *
     * @param object $gameFile
     *
     * @return bool
     */
    public function remove($gameFile): bool
    {
        GameFile::where('id', $gameFile->id)->delete();

        return true;
    }

    /**
     * Get file direct link.
     *
     * @param object $gameFile
     *
     * @return string
     */
    public function asset($gameFile)
    {
        return config('game-files.domain') . config('game-files.path') . $gameFile->type . '/' . $gameFile->name;
    }
}