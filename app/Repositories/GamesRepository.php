<?php

namespace App\Repositories;

use App\Models\UserGame;
use App\Http\Interfaces\GamesRepoInterface;

class GamesRepository implements GamesRepoInterface{
    public function addGameAccount($data){
        $result = UserGame::create($data);
        return $result;
    }
}