<?php

namespace App\Repositories;


use App\Http\Interfaces\MailingInterface;
use Illuminate\Support\Facades\Mail;

class MailingRepository implements MailingInterface
{
    public function senMail(string $title, string $content, string $subject, string $from = 'Site Admin', string $template = 'emails.default', $file = null, $locale = 'en'): void
    {
        Mail::send($template, ['title' => $title, 'content' => $content], function ($message) use ($file, $from, $subject) {

            $message->from($from);

            $message->to('mamad@outlook.com');
            if ($file) //Attach file
                $message->attach($file);

            //Add a subject
            $message->subject($subject);
        });
    }
}
