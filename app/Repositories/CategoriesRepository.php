<?php

namespace App\Repositories;

use App\Models\Category;
use App\Interfaces\CategoriesInterface;
use App\Models\GameFile;

class CategoriesRepository implements CategoriesInterface
{
    /**
     * @var array
     */
    protected $data = [];

    /**
     * CategoriesRepository constructor.
     */
    public function __construct()
    {
    }

    /**
     * Fetch category by id.
     *
     * @param $id
     *
     * @return mixed|void
     */
    public function fetchById($id)
    {
        return Category::find($id);
    }

    /**
     * Fetch categories by name.
     *
     * @param string $name
     * @param string $sortType
     * @param int $limit
     *
     * @return mixed
     */
    public function fetchByName(string $name, string $sortType = 'asc', int $limit = 100)
    {
        return Category::where('name', 'LIKE', '%' . $name . '%')->orderBy('name', $sortType)->limit($limit)->get();
    }


    public function returnAllCategories(){
        return Category::all();
    }

    /**
     * Set category name.
     *
     * @param string $name
     *
     * @return CategoriesRepository
     */
    public function setName(string $name): CategoriesRepository
    {
        $this->data['name'] = $name;

        return $this;
    }

    /**
     * @param string $imageName
     * @param mixed $id
     *
     * @return CategoriesRepository
     */
    public function setImage(string $imageName, $id = null): CategoriesRepository
    {
        $this->data['image'] = (object)['id' => $id, 'name' => $imageName];

        return $this;
    }

    /**
     * Save category in data storage.
     *
     * @param mixed $id
     *
     * @return Category
     */
    public function save($id = null): Category
    {
        if ($id) {
            $category = Category::find($id);

            $category->fill($this->data);

            $category->save();
        } else {
            $category = Category::create($this->data);
        }

        return $category;
    }

    /**
     * Store or update category.
     *
     * @param string $name
     * @param \Illuminate\Http\UploadedFile|null $image
     * @param int|null $id
     *
     * @return mixed|void
     *
     * @throws \App\Exceptions\BadUploadFileException
     */
    public function storeOrUpdate(string $name, $image = null, $id = null)
    {
        $gameFileRepository = new GameFilesRepository();

        $this->setName($name);

        if ($image) {

            $image = $gameFileRepository->upload($image, GameFile::TYPE_IMAGE);

            $this->setImage($image->name, $image->id);
        }

        return $this->save($id);
    }

    /**
     * Remove category from data storage.
     *
     * @param $id
     *
     * @param bool $forceDeletes
     */
    public function remove($id, bool $forceDeletes = false)
    {
        if ($forceDeletes) {
            $category = Category::find($id);

            if ($category) {
                $category->forceDelete();
            }
        } else {
            Category::destroy($id);
        }
    }
}