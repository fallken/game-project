<?php

namespace App\Repositories;

use App\Models\User;
use App\Traits\Utils;
use App\Models\MatchInvitation;
use App\Http\Interfaces\MatchInvitationRepoInterface;


class MatchInvitationRepository implements MatchInvitationRepoInterface{
    use Utils;
    public function sendMatchInvition($user_id,$target_user_id,$match_id,$side){
        $data=[
            'host_user_id'=>$user_id,
            'match_id'=>$match_id,
            'match_side'=>$side,
            'target_user_id'=>$target_user_id,
        ];
        $invitaion = MatchInvitation::create($data);
        return $invitaion;
    }
    #no specific guards will be set on these functions 
    public function rejectMatchInvitation($invitation_id){
        $invitation = MatchInvitation::findOrFail($invitation_id);

        $result = $invitation->update([
            'status'=>'rejected'
        ]); 

        return $result;
    }
    #again we will not have anny specific guards here (we will handle the guards on the controller )
    public function cancelMatchInvitation($invitation_id){
        $invitaion = MatchInvitation::findOrFail($invitation_id);

        $result = $invitation->update([
            'status'=>'cancelled'
        ]);

        return $result;
    }
}