<?php
namespace App\Repositories;

use App\Models\User;
use App\Models\Match;
use App\Traits\Utils;
use App\Models\GameAdmin;
use App\Jobs\AdminJoinedMatch;
use App\Models\MatchInvitation;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Interfaces\MatchesInterface;
use Illuminate\Database\Eloquent\Collection;
use App\Http\Interfaces\FinancialRepoInterface;

   
   
class MatchesRepository implements MatchesInterface
{
    use Utils;

    public $financial;
    public function __construct(FinancialRepoInterface $financial){
        $this->financial = $financial;
    }

    public function getMatches($skip=0,$take=null,$relations=null,$id=null,$game_id=null){
        $query= new Match();
        $query= isset($id)?$query->where('id',$id):$query;
        $query= !is_null($game_id)?$query->where('game_id',$game_id):$query;
        $query = isset($relations)?$query->with($relations):$query;
        $query = $query->skip($skip)->take($take??10);
        $matches = $query->get();
        return $matches;
    }

    public function joinUserToMatches($user_id,$match_id,$side,$user_account_id,$has_paid_for_game=false,$payment_transaction_id=null){
        $user = User::find($user_id);
        $match = Match::find($match_id);
        $match->update([
            'joined_users'=>$match->joined_users+1
        ]);
        #add the user to the match
        $result = $user->matches()->attach($match_id,['side'=>$side,
        'user_account_id'=>$user_account_id,//not the user name but the account id .the id in the user matches .  
        'has_paid_for_game'=>$has_paid_for_game,
        'payment_transaction_id'=>$payment_transaction_id
        ]);

        if($this->checkUserAlreadyInMatch($user,$match_id,$side)){
            return true;
        }else{
            return false;
        }
    }


    public function userMatches($user=null,$user_id=null,$relations=null){
      
    }


    public function getInvitations($user_id,$filter=false){
        $user_invitations = MatchInvitation::where('user_id',$user_id);
        if ($filter){
            $user_invitations->whereNotIn('status',['accepted','rejected','cancelled']);
        }
        return $user_invitations->get();
    }

    #cancelling the whole match using only its id and nothing less and nothing more
    public function deleteMatch($match_id,$adminSide=0){
        $match = Match::findOrFail($match_id);
        // exit(response()->json(['message'=>$match_id],200));
        $match_users = $this->getMatchUsersBySide($match_id);
        #check if  there was any money for the match or not . 
        if(!$match->price || $match->price == 0){
            #kicking all the users from the match list by detching the match from user matches table 
            $this->detachUsersFromMatch($match_id,$match_users['host_users']);
            $this->detachUsersFromMatch($match_id,$match_users['guest_users']);
            $res = $match->delete();
            return response()->json(['message'=>$res?'calcellation was successfull':'the process was not successfull'],200);   
        }
        #first checking if any of the main users has paid for the game?
        if( $adminSide == 0){ #delete the user side only if the user side is 0 meaning the user can delete bot sides other wise go only for one side . 
           if($match->host_pay == 'single'){#if the host has only paid for himself
            foreach($match_users['host_users'] as $user){
                #if($user->has_paid_for_game){#if the user has paid for the match 
                    //should check the coupons and other stuff it the user wants to check out the money and do a trick
                    $transaction_data = [
                        'user_id'=>$user->id,
                        'game_id'=>$match->game_id,
                        'match_id'=>$match->id,
                        'amount'=>$match->price,
                        'type'=>config('matches.transactionTypes.1'),#refound for the match
                        'desc'=>"deleting the whole match and paying user $user->name side of host the money of the game cause they paid it singly"
                    ];
                    $this->financial->addCreditToCustomer(null,$match->price,$user,$transaction_data);
                #}
            }
            $this->detachUsersFromMatch($match_id,$match_users['host_users']);
            }else{
                $host_payment = $match->host_side_players * $match->price;#the whole price of the user which will pay
                $user =$match_users['host_users']->where('id',$match->host_user_id)->first();
                $transaction_data = [
                        'user_id'=>$user->id,
                        'game_id'=>$match->game_id,
                        'match_id'=>$match->id,
                        'amount'=>$host_payment,
                        'type'=>config('matches.transactionTypes.1'),#refound for the match
                        'sdesc'=>"deleting the whole match and paying the whole money of the game to  user $user->name side of host the whole money of the game cause he paid it singly"
                    ];
                $this->financial->addCreditToCustomer(null,$host_payment,$user,$transaction_data);
                $this->detachUsersFromMatch($match_id,$user);
            }
        }
        if($match->guest_pay =='single'){#if the guest has only paid for himself
            foreach($match_users['guest_users'] as $user){
                //should check the coupons and other stuff it the user wants to check out the money and do a trick
                if($user->has_paid_for_game){//only if the user has paid for the match let him take the credit . 
                    $transaction_data = [
                        'user_id'=>$user->id,
                        'game_id'=>$match->game_id,
                        'match_id'=>$match->id,
                        'amount'=>$match->price,
                        'type'=>config('matches.transactionTypes.1'),#refound for the match
                        'desc'=>"deleting the whole match and paying user $user->name side of guest the money of the game cause they paid it singly"
                    ];
                    $this->financial->addCreditToCustomer(null,$match->price,$user,$transaction_data);
                }
            }
        }else{
            $guest_payment = $match->guest_side_players * $match->price;
            $user =$match_users['guest_users']->where('id',$match->guest_user_id)->first();
            $transaction_data = [
                    'user_id'=>$user->id,
                    'game_id'=>$match->game_id,
                    'match_id'=>$match->id,
                    'amount'=>$match->price,
                    'type'=>config('matches.transactionTypes.1'),#refound for the match
                    'sdesc'=>"deleting the whole match and paying the whole money of the game to  user $user->name side of guest the whole money of the game cause he paid it singly"
                ];
            $this->financial->addCreditToCustomer(null,$match->price,$user,$transaction_data);
        }
        #in the final state delete the match.
        $result = $match->delete();
        return $result;
    }

    #this funciton will delete the user from the match 
    public function deleteUserFromMatch($match_id,$user_id=null,$user=null){
        $customer = !is_null($user)?$user:User::findOrFail($user_id);
        $match = Match::findOrFail($match_id);
        $userside = $this->getSide($customer->id , $match_id);
        $userMatchRow = $this->getUserMatchRow($customer->id,$match_id);
        $transaction_data = [
                    'user_id'=>$customer->id,
                    'game_id'=>$match->game_id,
                    'match_id'=>$match->id,
                    'amount'=>$match->price,
                    'type'=>config('matches.transactionTypes.1'),#refound for the match
                    'desc'=>"deleting user $customer->name and side of $userside and refounding the money paid for the match"
                ];
        if($userside == 0){#if he is one the host side and if the user has already paid for the match then add to his credit
            if($match->price !== 0 && !is_null($match->price) && $match->host_pay == 'single' && $userMatchRow->has_paid_for_game ){#if the guest has only paid for himself and the user has already paid the price of the match
                //should check the coupons and other stuff it the user wants to check out the money and do a trick
                $this->financial->addCreditToCustomer(null,$match->price,$customer,$transaction_data);
                $this->detachUsersFromMatch($match_id,null,$customer);
            }else{
                $this->detachUsersFromMatch($match_id,null,$customer);
            }   
        }
        else{
           if($match->price !== 0 && !is_null($match->price) && $match->guest_pay == 'single' && $userMatchRow->has_paid_for_game ){#if the guest has only paid for himself and the user has already paid the price of the match
                //should check the coupons and other stuff it the user wants to check out the money and do a trick
                $this->financial->addCreditToCustomer(null,$match->price,$customer,$transaction_data);
                $this->detachUsersFromMatch($match_id,null,$customer);
            }else{
                $this->detachUsersFromMatch($match_id,null,$customer);
            }
        }
        return true;
    }

    public function detachUsersFromMatch($match_id,$users=null,$user=null){#pass the users object on collection like array of objects
        $match = Match::find($match_id);
        if($users){
            foreach($users as $userItem){
                $userItem->matches()->detach($match_id);
                $match->update([
                    'joined_users'=>$match->joined_users - 1
                ]);
             }
        }else{
            $user->matches()->detach($match_id);
            $match->update([
                    'joined_users'=>$match->joined_users - 1
            ]);
        }
        return true;
    }

    public function kickMatchSide($match_id,$side){
        $match = Match::findOrFail($match_id);
        $users = $this->getMatchUsersBySide($match_id,$side);
        #check if  there was any money for the match or not . 
        if(!$match->price || $match->price == 0){
            #kicking all the users from the match list by detching the match from user matches table 
            $res = $this->detachUsersFromMatch($match_id,$users);
            return response()->json(['message'=>$res?'calcellation was successfull':'the process was not successfull'],200);   
        }
        if($match[$side == 0?'host_pay':'guest_pay'] == 'single'){#if the host has only paid for himself
            foreach($users as $user){
                //should check the coupons and other stuff it the user wants to check out the money and do a trick
                $transaction_data = [
                    'user_id'=>$user->id,
                    'game_id'=>$match->game_id,
                    'match_id'=>$match->id,
                    'amount'=>$match->price,
                    'type'=>config('matches.transactionTypes.1'),#refound for the match
                    'desc'=>"kick a match side of $match->id and user of $user->id with name of $user->name"
                ];
                $this->financial->addCreditToCustomer(null,$match->price,$user,$transaction_data);
            }
            $this->detachUsersFromMatch($match_id,$users);
        }else{
            $host_payment = $match[$side==0?'host_side_players':'guest_side_players'] * $match->price;#the whole price of the user which will pay
            $user =$users->where('id',$match->host_user_id)->first();
            $transaction_data = [
                    'user_id'=>$user->id,
                    'game_id'=>$match->game_id,
                    'match_id'=>$match->id,
                    'amount'=>$host_payment,
                    'type'=>config('matches.transactionTypes.1'),#refound for the match
                    'desc'=>"deleting the whole match and paying the whole money of the game to  user $user->name side of host the whole money of the game cause he paid it singly"
                ];
            $this->financial->addCreditToCustomer(null,$host_payment,$user,$transaction_data);
            $this->detachUsersFromMatch($match_id,$user);
        }
    }

    public function inviteUser($user_id,$target_user_id,$match_id,$match_side){
        $data=[
            'host_user_id'=>$user_id,
            'target_user_id'=>$target_user_id,
            'match_id'=>$match_id,
            'match_side'=>$match_side
        ];
        $result = MatchInvitation::create($data);
        //todo: broadcast the socket event for new user invitation
        return $result;
    }
    #remove the admin from the match 
    public function removeAdminFromTheMatch($match_id,$admin_id){
        $match = Match::find($match_id);

        $result = $match->update([
            'admin_id'=>$admin_id
        ]);

        return $result;
    }
    #this method will only join the admin to the match , it does not have any thinking 
    public function joinAdminToMatch($match_id , $admin_id){
        $match = Match::find($match_id);
        $result = $match->update([
            'admin_id'=>$admin_id
        ]);
        if($result){
            dispatch(new AdminJoinedMatch);
        }
        return $result;
    }
    #will set the end status to the function
    public function setMatchFinishStatus($match_id,$side,$status,$admin_id){
        $winner_side_result = false;
        #$loser_side_result = false;
        $match = Match::find($match_id);
        $admin = GameAdmin::find($admin_id);

        return DB::transaction(function() use ($winner_side_result,$match,$admin,$side){
            $winner_side_result = $this->handleWinnerSide(null,$match,$side);
            #pay the admin for his deeds
            $admin_payment_result = $this->handleAdminCredit(null,$match,$side);
            #$loser_side_result = $this->handleLoserSide(null,$match,$side);
            if($winner_side_result && $admin_payment_result){
                $match->update([
                    'status'=>'finished',
                    'winner_side'=>$side
                ]);
                $admin->update([
                    'games'=>$admin->games+1
                ]);
                return true;
            }
        });

    }

    public function changeMatchStatus($match_id=null,$match=null,$status){
      $match = is_null($match)?Match::find($match_id):$match;
      $result = $match->update([
          'status'=>$status
      ]);
      return $result;
    }

    public function addGameAdminCredit($match_id=null,$match=null,$amount=null){
     $match = is_null($match)?Match::find($match_id):$match;
     if(is_null($amount)){
         $amount  = $this->financial->getApplicationShare($match_id);
     }
    
    }
    #will do all the transactions for all the users on one side.
    public function handleWinnerSide($match_id=null,$match=null,$side){
        $match = is_null($match)?Match::find($match_id):$match;
        $winner_side_users = $this->getMatchUsersBySide($match->id,$side);
        $loser_side_users = $this->getMatchUsersBySide($match->id,$side==0?1:0);
        #calculate the share for the application 
        $application_share_price = $this->financial->getApplicationShare(null,$match);
        $winner_payment_money = $match->price+($match->price-$application_share_price)*count($loser_side_users)/count($winner_side_users);
        if(!$match->price || $match->price == 0){
            #kicking all the users from the match list by detching the match from user matches table 
            #$this->detachUsersFromMatch($match_id,$winner_side_users);
        }
        #if the users have paid for their price in the match
        else if($match[$side==0?'host_pay':'guest_pay'] == 'single'){
            foreach($winner_side_users as $user){
                //should check the coupons and other stuff it the user wants to check out the money and do a trick
                $transaction_data = [
                    'user_id'=>$user->id,
                    'game_id'=>$match->game_id,
                    'match_id'=>$match->id,
                    'amount'=>$winner_payment_money,
                    'type'=>config('matches.transactionTypes.4'),#refound for the match
                    'desc'=>"adding the credit for the winner side of $side and user with the id of $user->id"
                ];
                $this->financial->addCreditToCustomer(null,$winner_payment_money,$user,$transaction_data);
            }
            #$this->detachUsersFromMatch($match_id,$users);
        }
        else if($match[$side==0?'host_pay':'guest_pay'] == 'all'){
            #charge only the the game side admin .

            $payment_value = $match[$side==0?'host_side_players':'guest_side_players'] * $winner_payment_money;#the whole price of the user which will pay
            $user =$winner_side_users->where('id', $match[$side==0?'host_user_id':'guest_user_id'])->first();
            $transaction_data = [
                    'user_id'=>$user->id,
                    'game_id'=>$match->game_id,
                    'match_id'=>$match->id,
                    'amount'=>$payment_value,
                    'type'=>config('matches.transactionTypes.4'),#refound for the match
                    'desc'=>"paying the whole money to the user $user->id with name of $user->name and side of $side cause he paid for the whole game "
            ];
            $this->financial->addCreditToCustomer(null,$payment_value,$user,$transaction_data);
            #$this->detachUsersFromMatch($match_id,$user);    
        }
        return true;
    }
    #will give the share of the admin if there was actually an admin in the game . 
    public function handleAdminCredit($match_id=null,$match=null,$side){
         $match = is_null($match)?Match::find($match_id):$match;
         if(!$match->admin_id){#if the admin id is null
            return true;
         }
        $loser_side_users = $this->getMatchUsersBySide($match->id,$side==0?1:0);
        $admin_share_from_match = count($loser_side_users)*$this->financial->calculateAdminPrice(null,$match);
         $transaction_data = [
            'user_id'=>'',
            'game_id'=>$match->game_id,
            'match_id'=>$match->id,
            'amount'=>$admin_share_from_match,
            'type'=>config('matches.transactionTypes.7'),#adminIncome from the match
            'desc'=>"adding the admin income value of $admin_share_from_match to admin id of $match->admin_id and match of $match->id with the game of $match->game_id "
         ];

         $result = $this->financial->addAdminCredit($match->admin_id,null,$admin_share_from_match,$transaction_data);
         if($result){
             return true;
         }
         return false;
    }
    #will handle the looser side and the transactions for the winner side 
    public function handleLoserSide($match_id,$side){

    }
}