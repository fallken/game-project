<?php

namespace App\Repositories;

use App\Models\User;
use App\Models\Match;
use App\Traits\Utils;
use App\Models\UserTransaction;
use App\Http\Interfaces\FinancialRepoInterface;

class FinancialRepository implements FinancialRepoInterface{
    use Utils;
    #add credit to custome like credit of winning a match
    public function addCreditToCustomer($customer_id=null,$credit,$user=null,$transaction_data=null){
        $customer = !is_null($user)?$user:User::findOrFail($customer_id);
        #updating the customer result of it . 
        $result = $customer->update([
            'wallet'=>$customer->wallet + $credit,
        ]);
        if(!is_null($transaction_data)){
            $this->createTransaction($transaction_data);
        }
    }
    #reducing the credit from the customer
    public function reduceCreditFromCustomer($customer_id=null,$credit,$user=null,$transaction_data=null){
        $customer = !is_null($user)?$user:User::findOrFail($customer_id);
        #updating the customer result of it . 
        $result =  $customer->update([
            'wallet'=>$customer->wallet - $credit,
        ]); 
        if(!is_null($transaction_data)){
            $this->createTransaction($transaction_data);
        }
    }
    #calculate the admin share from the match . the percent value is actually the share of the admin of the game
    public function calculateAdminPrice($match_id=null,$match=null,$percent=0.05){
        $match = is_null($match)?Match::find($match_id):$match;
        #i should probably check the amount of the match so that it wouldnt pass specific value 
        #calculate the share of the admin
//        $percent = 1-$percent;
        return $match->price * $percent;
    }
    #reduce the share of the application
    public function getApplicationShare($match_id=null,$match=null,$percent=0.1){
        $match = is_null($match)?Match::find($match_id):$match;
        #i should probably check the amount of the match so that it wouldnt pass specific value 
        #calculate the share of the admin
        #$percent = 1-$percent;
        return $match->price * $percent;
    }
    #create transactions regarding the matches and other stuff
    public function createTransaction($data){
        #create the transaction
        $result = UserTransaction::create($data);
        
        return $result;
    }

    public function reduceAdminCredit($admin_id=null,$admin=null,$credit,$transaction_data=null){
        $user = $this->getUserFromAdminId($admin_id);
        #updating the customer result of it .
        $result = $user->update([
            'wallet'=>$user->wallet - $credit,
        ]);
        if(!is_null($transaction_data)){
            $transaction_data['user_id']=$user->id;
            $this->createTransaction($transaction_data);
        }
    }

    public function addAdminCredit($admin_id=null,$admin=null,$credit,$transaction_data=null){
        $user = !is_null($admin)?$admin->user:$this->getUserFromAdminId($admin_id);
        #updating the customer result of it . 
        $result =  $user->update([
            'wallet'=>$user->wallet + $credit,
        ]); 
        if(!is_null($transaction_data)){
            $transaction_data['user_id']=$user->id;#cause the user is not available out there in some cases. 
            $this->createTransaction($transaction_data);
        }
        return true;
    }
}