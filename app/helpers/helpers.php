<?php

use App\Models\User;


if (! function_exists('getUserByIdOrObject')) {
    /**
     * return the user using id  
     *
     * @param  arrary $selects
     * @return App\Models\User;
     */
    function getUserByIdOrObject($user){
        if(isset($user->name)){#the first condition is if the user is object of the users table or not 
            return $user;
        }
        #the second condition is if the user is integer or not 
        return User::findOrFail($user);
    }
}

if (! function_exists('getUserQuery')) {
    /**
     * return a user model object with preselected fields . 
     *
     * @param  arrary $selects
     * @return App\Models\User;
     */
    function getUserQuery($selects=null){
        $user = new User;
        $user->select(['name','email','avatar']);
        return $user;
    }
}