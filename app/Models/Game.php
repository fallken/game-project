<?php

namespace App\Models;

use App\Models\User;
use App\Models\Category;
use App\Models\GameAdmin;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Game extends Model
{
    use SoftDeletes;

    const STATUS_ACTIVE = 'active';
    const STATUS_DISABLE = 'disable';

    public function users(){
        return $this->belongsToMany(User::class,'user_games');
    }

    public function categories(){
        return $this->belongsToMany(Category::class,'category_games')->withTimestamps();
    }

    public function admins(){
        return $this->hasMany(GameAdmin::class,'game_id','id');
    }
}
