<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserTransaction extends Model
{
    const TYPE_REFUND = 'refund';
    const TYPE_CHARGE = 'charge';
    const TYPE_GIFT = 'gift';
    const TYPE_INCOME = 'income';
    const TYPE_PUNISH = 'punish';
    const TYPE_EXPENSE = 'expense';

    protected $fillable = ['user_id','game_id','match_id','admin_id','transaction_id','amount','currency','type','desc'];
    
}
