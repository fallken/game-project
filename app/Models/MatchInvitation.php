<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MatchInvitation extends Model
{
    protected $fillable = ['host_user_id','match_id','match_side','target_user_id','status'];

    public function target(){
        return $this->belongsTo(User::class,'target_user_id','id');
    }

    public function owner(){
        return $this->belongsTo(User::class,'host_user_id','id');
    }

}
