<?php

namespace App\Models;

use App\Models\Game;
use App\Models\Match;
use App\Traits\General;
use App\Models\GameAdmin;
use App\Models\UserFriend;
use App\Traits\Utils;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Notifications\MailResetPasswordNotification;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, HasRoles, SoftDeletes, HasApiTokens,Utils;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'mobile', 'wallet', 'max_friend_no', 'avatar', 'password', 'email_verification_token', 'status', 'email_verified_at', 'provider', 'provider_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'email_verification_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return ['user_id' => $this->id, 'name' => $this->name ,'friends'=>$this->getFriendIds($this->id)];
    }


    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MailResetPasswordNotification($token));
    }

    public function games()
    {
        $pivotColumns = ['id', 'account_user_name', 'account_id', 'account_details', 'account_level', 'user_wallet', 'account_games', 'wins', 'losses', 'status'];
        return $this->belongsToMany(Game::class, 'user_games')->withPivot($pivotColumns)->withTimestamps();
    }

    public function matches()
    {
        $pivotColumns = ['user_account_id', 'has_paid_for_game', 'payment_transaction_id', 'side'];
        return $this->belongsToMany(Match::class, 'user_matches')->withPivot($pivotColumns)->withTimestamps();
    }

    public function friends()
    {
        $friends = UserFriend::where('user_id', $this->id)->where('status', 'accepted')->pluck('friend_id');
        $friends = User::select('name', 'email', 'avatar')->whereIn('id', $friends)->get();
        return $friends;
    }

    public function messages($offset = 0)
    {
        $owning = Message::where('user_id', $this->id)->orderBy('created_at', 'desc')->skip($offset * 10)->take(10)->get();
        $targetted = Message::where('target_user_id', $this->id)->orderBy('created_at', 'desc')->skip($offset * 10)->take(10)->get();
        return [
            'owner' => $owning,
            'targetted' => $targetted
        ];
    }

    public function gameAdmin()
    {
        return $this->hasMany(GameAdmin::class);
    }
    /* 
        //the request which are made by the user for making friends . 
        public function friendReuqestTarget(){
            return $this->hasMany(UserFriend::class,'friend_id','id');
        }
        //the request which are made for making friend with the user 
        public function friendReuqestOwner(){
            return $this->hasMany(UserFriend::class,'user_id','id');
        }
    */
}