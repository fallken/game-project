<?php

namespace App\Models;

use App\Models\Game;
use App\Models\User;
use App\Models\Match;
use Illuminate\Database\Eloquent\Model;

class GameAdmin extends Model
{
    const STATUS_ENABLE = 'enable';
    const STATUS_BANNED = 'banned';
    const STATUS_REJECTED = 'rejected';
    const STATUS_UNVERIFIED = 'unverified';

    protected $fillable = ['user_id','game_id','games','level','rate','status','account_id'];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function game(){
        return $this->belongsTo(Game::class,'game_id','id');
    }

    public function matches(){
        return $this->hasMany(Match::class);
    }
}
