<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserFriend extends Model
{
    protected $fillable = ['user_id','friend_id','accepted_at','status'];
    const STATUS_ACCEPTED = 'accepted';
    const STATUS_PENDING = 'pending';
    const STATUS_REJECTED = 'rejected';

    public function requestOwner()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }
}
