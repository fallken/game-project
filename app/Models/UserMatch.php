<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserMatch extends Model
{
    protected $fillable=['match_id','user_id','side','user_account_id', 'has_paid_for_game', 'payment_transaction_id'];

    
}
