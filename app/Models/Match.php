<?php

namespace App\Models;

use App\Models\GameAdmin;
use Illuminate\Database\Eloquent\Model;

class Match extends Model
{
    protected $fillable=['joined_users','game_id','creator_id','host_user_id','guest_user_id'
    ,'joined_users','price','currency','status','max_players',
    'min_players','host_side_players','guest_side_players','admin_id',
    'message','start_time','min_level_allowed','max_level_allowed','winner_side','host_pay','guest_pay','host_team_id','guest_team_id','tournament_id'];
    const STATUS_PENDING = 'pending';
    const STATUS_ONGOING = 'ongoing';
    const STATUS_FINISHED = 'finished';
    const STATUS_STOPPED = 'stopped';
    const STATUS_FAILED = 'failed';

    const HOST_PAY_SINGLE = 'single';
    const HOST_PAY_ALL = 'all';

    const GUEST_PAY_SINGLE = 'single';
    const GUEST_PAY_ALL = 'all';

    const WINNER_SIDE_HOST = 'host';
    const WINNER_SIDE_GUEST = 'guest';


    public function users()
    {
        $pivotColumns = ['side','match_id','user_id','has_paid_for_game'];

        return $this->belongsToMany(User::class, 'user_matches')->withPivot($pivotColumns)->withTimestamps();
    }

    public function admin(){
        return $this->belongsTo(GameAdmin::class);
    }

    public function creator(){
        return $this->belongsTo(User::class,'creator_id','id');
    }

    public function game(){
        return $this->belongsTo(Game::class,'game_id','id');
    }
}
