<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserGame extends Model
{
    protected $fillable=['account_user_name','game_id','user_id','account_id', 'account_level', 'account_details', 'user_wallet', 'account_games', 'wins','losses', 'status'];
    const STATUS_ALLOW = 'allow';
    const STATUS_DISALLOW = 'disallow';
}
