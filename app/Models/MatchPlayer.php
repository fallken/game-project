<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MatchPlayer extends Model
{
    const SIDE_HOST = 'host';
    const SIDE_GUEST = 'guest';
}
