<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Punishment extends Model
{
    const TYPE_PUNISH = 'punish';
    const TYPE_GIFT = 'gift';
}
