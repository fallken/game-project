<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Message extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'user_id','match_id','match_side_id','admin_id','super_admin_id','room_id','target_user_id','text','type','seen_at'
    ];

    const TYPE_SYSTEM = 'system';
    const TYPE_TALK = 'talk';

    public function msgOwner(){
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function msgTarget(){
        return $this->belongsTo(User::class,'target_user_id','id');
    }

    public function matchTarget()
    {
        return $this->belognsTo(Match::class,'match_id','id');
    }

    public function gameAdmin()
    {
        return $this->belongsTo(GameAdmin::class,'admin_id','id');
    }
}
