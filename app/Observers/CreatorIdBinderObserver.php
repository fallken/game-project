<?php

namespace App\Observers;

class CreatorIdBinderObserver
{
    /**
     * Handle the model "creating" event.
     *
     * @param $model
     * @return void
     */
    public function creating($model)
    {
        $model->creator_id = auth()->id() ?? 0;
    }

    /**
     * Handle the model "created" event.
     *
     * @param $model
     * @return void
     */
    public function created($model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param $model
     * @return void
     */
    public function updated($model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param $model
     * @return void
     */
    public function deleted($model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  $model
     * @return void
     */
    public function restored($model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param $model
     * @return void
     */
    public function forceDeleted($model)
    {
        //
    }
}
