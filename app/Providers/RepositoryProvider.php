<?php

namespace App\Providers;

use App\Repositories\GamesRepository;
use App\Repositories\MailingRepository;
use App\Repositories\MatchesRepository;
use Illuminate\Support\ServiceProvider;
use App\Http\Interfaces\MailingInterface;
use App\Http\Interfaces\MatchesInterface;
use App\Repositories\FinancialRepository;
use App\Http\Interfaces\GamesRepoInterface;
use App\Http\Interfaces\FinancialRepoInterface;
use App\Repositories\MatchInvitationRepository;
use App\Http\Interfaces\MatchInvitationRepoInterface;

class RepositoryProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(MailingInterface::class,MailingRepository::class);
        $this->app->singleton(MatchesInterface::class,MatchesRepository::class);
        $this->app->singleton(MatchInvitationRepoInterface::class,MatchInvitationRepository::class);
        $this->app->singleton(FinancialRepoInterface::class,FinancialRepository::class);
        $this->app->singleton(GamesRepoInterface::class,GamesRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
