<?php

namespace App\Providers;

use App\Models\Category;
use App\Models\GameFile;
use App\Observers\CreatorIdBinderObserver;

use Illuminate\Support\ServiceProvider;

class CreatorIdBinderProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        GameFile::observe([CreatorIdBinderObserver::class]);
        Category::observe([CreatorIdBinderObserver::class]);
    }
}
