<?php

namespace App\Http\Middleware;

use Closure;
use App\Traits\Utils;

class CheckAuthenticatedUsers
{
     use Utils;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$role)
    {   
        if($role === 'isGameAdmin'){
            $is_game_admin = $this->checkIsAdmin($request->user()->id);
            if(!$is_game_admin){#if the user is actually a game admin then let him pass otherwise dont let him do anything . 
                return response()->json(['message'=>'user is not allowed to access these routes'],403);
            }
        }
        return $next($request);
    }
}
