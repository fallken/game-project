<?php


namespace App\Http\Interfaces;


interface MailingInterface
{
    public function senMail(string $title, string $content, string $subject, string $from = 'Site Admin', string $template = 'emails.default', $file = null, $locale = 'en'): void;
}
