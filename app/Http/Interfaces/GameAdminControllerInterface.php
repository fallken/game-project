<?php
namespace App\Http\Interfaces;
use Illuminate\Http\Request;

interface GameAdminControllerInterface{
   /**
   * @OA\Post(
   *     path="/api/adminJoinMatch",
   *     operationId="/adminJoinMatch",
    *     security={ {"bearer": {}} },
   *     tags={"game admin section"},
   *     @OA\Parameter(
   *         name="match_id",
   *         in="query",
   *         description="the match id of which the game admin wants to join",
   *         required=true,
   *         @OA\Schema(type="string", default="1")
   *     ),
   *     @OA\Parameter(
   *         name="admin_id",
   *         in="query",
   *         description="the specific id of the admin for joining the game . ",
   *         required=true,
   *         @OA\Schema(type="string", default="12")
   *     ),
   *     @OA\Response(
   *      @OA\MediaType(mediaType="application/json"),response="200",
   *      description="will return success response for the user",
   *     ),
   * )
   *
   * @param Request $request
   *
   * @return mixed
   */
    public function adminJoinMatch(Request $reqeust);

    /**
   * @OA\Post(
   *     path="/api/adminLeaveMatch",
   *     operationId="/adminLeaveMatch",
     *     security={ {"bearer": {}} },
   *     tags={"game admin section"},
   *     @OA\Parameter(
   *         name="match_id",
   *         in="query",
   *         description="the match id of which the game admin wants to join",
   *         required=true,
   *         @OA\Schema(type="string", default="1")
   *     ),
   *     @OA\Response(
   *      @OA\MediaType(mediaType="application/json"),response="200",
   *      description="will return response of the user for leaving match",
   *     ),
   * )
   *
   * @param Request $request
   *
   * @return mixed
   */
    public function adminLeaveMatch(Request $request);

     /**
     * @OA\Post(
     *     path="/api/adminStartMatch",
     *     operationId="/adminStartMatch",
      *     security={ {"bearer": {}} },
     *     tags={"game admin section"},
     *     @OA\Parameter(
     *         name="match_id",
     *         in="query",
     *         description="the match id of which the game admin wants to join",
     *         required=true,
     *         @OA\Schema(type="string", default="1")
     *     ),
     *     @OA\Parameter(
     *         name="admin_id",
     *         in="query",
     *         description="the id of the admin that the game admin wants to judge the match with",
     *         required=true,
     *         @OA\Schema(type="string", default="4")
     *     ),
     *     @OA\Response(
     *      @OA\MediaType(mediaType="application/json"),response="200",
     *      description="will return the response for starting the match of the user",
     *     ),
     * )
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function adminStartMatch(Request $request);

    /**
     * @OA\Post(
     *     path="/api/adminEndMatch",
     *     operationId="/adminEndMatch",
     *     security={ {"bearer": {}} },
     *     tags={"game admin section"},
     *     @OA\Parameter(
     *         name="match_id",
     *         in="query",
     *         description="the match id of which the game admin wants to join",
     *         required=true,
     *         @OA\Schema(type="string", default="1")
     *     ),
     *     @OA\Parameter(
     *         name="admin_id",
     *         in="query",
     *         description="the id of the current match",
     *         required=true,
     *         @OA\Schema(type="string", default="1")
     *     ),
     *     @OA\Parameter(
     *         name="winner_side",
     *         in="query",
     *         description="the side of the winner which has won the game ",
     *         required=true,
     *         @OA\Schema(type="string", default="1")
     *     ),
     *     @OA\Response(
     *      @OA\MediaType(mediaType="application/json"),response="200",
     *      description="will end the match and seperate the shares among the users.",
     *     ),
     * )
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function endMatch(Request $request);

    /**
     * @OA\Post(
     *     path="/api/adminPauseMatch",
     *     operationId="/adminPauseMatch",
     *     security={ {"bearer": {}} },
     *     tags={"game admin section"},
     *     @OA\Parameter(
     *         name="match_id",
     *         in="query",
     *         description="the match id of which the game admin wants to join",
     *         required=true,
     *         @OA\Schema(type="string", default="1")
     *     ),
     *     @OA\Response(
     *      @OA\MediaType(mediaType="application/json"),response="200",
     *      description="will return pausing the match result ",
     *     ),
     * )
     *
     * @param Request $request
     *
     * @return mixed
     */    
    public function pauseMatch(Request $request);

    /**
     * @OA\Post(
     *     path="/api/adminChangeMatchStatus",
     *     operationId="/adminChangeMatchStatus",
     *     security={ {"bearer": {}} },
     *     tags={"game admin section"},
     *     @OA\Parameter(
     *         name="match_id",
     *         in="query",
     *         description="the match id of which the game admin wants to join",
     *         required=true,
     *         @OA\Schema(type="string", default="1")
     *     ),
     *     @OA\Response(
     *      @OA\MediaType(mediaType="application/json"),response="200",
     *      description="will return changing match status result ",
     *     ),
     * )
     *
     * @param Request $request
     *
     * @return mixed
     */    
    public function changeMatchStatus(Request $request);



    /**
     * @OA\Post(
     *     path="/api/requestForGameAdmin",
     *     operationId="/requestForGameAdmin",
     *     security={ {"bearer": {}} },
     *     tags={"game admin section"},
     *     @OA\Parameter(
     *         name="game_id",
     *         in="query",
     *         description="the id of the game that the user wants to be registered as its admin",
     *         required=true,
     *         @OA\Schema(type="string", default="1")
     *     ),
     *     @OA\Parameter(
     *         name="account_id",
     *         in="query",
     *         description="the account id of the game which the user wants to become the game admin with it",
     *         required=true,
     *         @OA\Schema(type="string", default="3")
     *     ),
     *     @OA\Response(
     *      @OA\MediaType(mediaType="application/json"),response="200",
     *      description="the route for requesting registration for becoming the game admin",
     *     ),
     * )
     *
     * @param Request $request
     *
     * @return mixed
     */     
    public function requestForGameAdmin(Request $request);


    /**
     * @OA\Post(
     *     path="/api/kickMatchUser",
     *     operationId="/kickMatchUser",
     *     security={ {"bearer": {}} },
     *     tags={"game admin section"},
     *     @OA\Parameter(
     *         name="match_id",
     *         in="query",
     *         description="the id of the match in which the admin wants to kick the user",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="admin_id",
     *         in="query",
     *         description="the admin id for checking if the user really is the admin or not",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="user_id",
     *         in="query",
     *         description="id of the user which is supposed to be kicked out of the match",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *      @OA\MediaType(mediaType="application/json"),response="200",
     *      description="the route for kicking specific user from the match",
     *     ),
     * )
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function kickMatchUser(Request $request);
}   