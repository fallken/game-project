<?php

namespace App\Http\Interfaces;

use Illuminate\Http\Request;


interface GameControllerInterface{

    /**
     * @OA\GET(
     *     path="/api/games",
     *     operationId="/games",
     *     security={ {"bearer": {}} },
     *     tags={"games section"},
     *     @OA\Parameter(
     *         name="categoryId",
     *         in="path",
     *         description="Category id",
     *         required=false,
     *         @OA\Schema(type="string", default="")
     *     ),
     *     @OA\Response(
     *      @OA\MediaType(mediaType="application/json"),response="200",
     *      description="will return the games if there is no catogory id available other wise will return games with the same category for them.",
     *     ),
     * )
     *
     * Get single category
     *
     * @param $categoryId
     *
     * @return mixed
     */
    public function index(Request $request);
}