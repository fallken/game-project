<?php

namespace App\Http\Interfaces;

use Illuminate\Http\Request;

interface UserControllerInterface{
    /**
     * @OA\Post(
     *     path="/api/searchUsers",
     *     operationId="/searchUsers",
     *     security={ {"bearer": {}} },
     *     tags={"users section"},
     *     @OA\Parameter(
     *         name="user_name",
     *         in="query",
     *         description="the name or email of the user ",
     *         required=true,
     *         @OA\Schema(type="string", default="farmaarz")
     *     ),
     *     @OA\Response(
     *      @OA\MediaType(mediaType="application/json"),response="200",
     *      description="will search for the user in the list.",
     *     ),
     * )
     *
     * @param Request $request
     *
     * @return mixed
    */  
    public function searchUsers(Request $request);



    

    /**
     * @OA\Get(
     *     path="/api/getUserProfile",
     *     operationId="/getUserProfile",
     *     security={ {"bearer": {}} },
     *     tags={"users section"},
     *     @OA\Response(
     *      @OA\MediaType(mediaType="application/json"),response="200",
     *      description="will return the profile of specific user .",
     *     ),
     * )
     *
     * @param Request $request
     *
     * @return mixed
    */  
    public function getUserProfile(Request $request);



     /**
     * @OA\Post(
     *     path="/api/addFriend",
     *     operationId="/addFriend",
      *     security={ {"bearer": {}} },
     *     tags={"users section"},
     *     @OA\Parameter(
     *         name="friend_id",
     *         in="query",
     *         description="the id of the friend to be requested",
     *         required=true,
     *         @OA\Schema(type="string", default="2")
     *     ),
     *     @OA\Response(
     *      @OA\MediaType(mediaType="application/json"),response="200",
     *      description="will send the user the friend request invitation ",
     *     ),
     * )
     *
     * @param Request $request
     *
     * @return mixed
    */  
     public function addFriend(Request $reqeust);




     /**
     * @OA\Post(
     *     path="/api/updateFriendRequest",
     *     operationId="/updateFriendRequest",
      *     security={ {"bearer": {}} },
     *     tags={"users section"},
     *     @OA\Parameter(
     *         name="request_id",
     *         in="query",
     *         description="the id of the request which the user has invitation",
     *         required=true,
     *         @OA\Schema(type="string", default="2")
     *     ),
     *     @OA\Parameter(
     *         name="status",
     *         in="query",
     *         description="the status contains : rejected , accepted",
     *         required=true,
     *         @OA\Schema(type="string", default="accepted")
     *     ),
     *     @OA\Response(
     *      @OA\MediaType(mediaType="application/json"),response="200",
     *      description="will let the user accept the friend request sent for him",
     *     ),
     * )
     *
     * @param Request $request
     *
     * @return mixed
    */     
     public function setFriendReuqestStatus(Request $request);




     /**
     * @OA\Get(
     *     path="/api/friendRequests",
     *     operationId="/friendRequests",
      *     security={ {"bearer": {}} },
     *     tags={"users section"},
     *     @OA\Response(
     *      @OA\MediaType(mediaType="application/json"),response="200",
     *      description="will return the list of the user requests either the sent ones and the received ones",
     *     ),
     * )
     *
     * @param Request $request
     *
     * @return mixed
    */      
     public function friendRequests(Request $request);




    /**
     * @OA\Post(
     *     path="/api/sendMessage",
     *     operationId="/sendMessage",
     *     security={ {"bearer": {}} },
     *     tags={"users section"},
     *     @OA\Parameter(
     *         name="target_user_id",
     *         in="query",
     *         description="the id of the user want to send the message to",
     *         required=true,
     *         @OA\Schema(type="string", default="2")
     *     ),
     *     @OA\Parameter(
     *         name="message_body",
     *         in="query",
     *         description="the message body of which you want to send the user the message",
     *         required=true,
     *         @OA\Schema(type="string", default="hi my friend are you alright and fine now?")
     *     ),
     *     @OA\Response(
     *      @OA\MediaType(mediaType="application/json"),response="200",
     *      description="will let the user accept the friend request sent for him",
     *     ),
     * )
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function sendMessageToUser(Request $request);



    /**
     * @OA\Get(
     *     path="/api/userMessages",
     *     operationId="/userMessages",
     *     security={ {"bearer": {}} },
     *     tags={"users section"},
     *     @OA\Response(
     *      @OA\MediaType(mediaType="application/json"),response="200",
     *      description="return the list of user message , owning or the targetting ones",
     *     ),
     * )
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function userMessages(Request $request);
}