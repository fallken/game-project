<?php
namespace App\Http\Interfaces;

use Illuminate\Database\Eloquent\Collection;


    interface MatchesInterface{
        public function getMatches($take=0 , $skip,$relations=null,$id=null);
        public function joinUserToMatches($user_id,$match_id,$side,$user_account_id,$has_paid_for_game=false,$payment_transaction_id=null);
        public function getInvitations($user_id,$filter=false);
        public function userMatches($user=null,$user_id=null,$relations=null);
        public function setMatchFinishStatus($match_id,$side,$status,$admin_id);
        public function changeMatchStatus($match_id=null,$match=null,$status);
        public function deleteUserFromMatch($match_id,$user_id=null,$user=null);
    }