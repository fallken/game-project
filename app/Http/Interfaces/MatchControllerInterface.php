<?php

namespace App\Http\Interfaces;

use Illuminate\Http\Request;
use App\Http\Requests\API\EditMatchRequest;
use App\Http\Requests\Api\JoinMatchRequest;
use App\Http\Requests\API\CreateMatchRequest;


interface MatchControllerInterface{
    /**
     * @OA\Post(
     *     path="/api/matches",
     *     operationId="/matches",
     *     security={ {"bearer": {}} },
     *     tags={"matches section"},
     *     @OA\Parameter(
     *         name="skip",
     *         in="query",
     *         description="offset value for the query ",
     *         required=false,
     *         @OA\Schema(type="string", default="0")
     *     ),
     *     @OA\Parameter(
     *         name="take",
     *         in="query",
     *         description="the number of records that we want to take ",
     *         required=false,
     *         @OA\Schema(type="string", default="10")
     *     ),
     *     @OA\Parameter(
     *         name="game_id",
     *         in="query",
     *         description="the id of the game with the images",
     *         required=false,
     *         @OA\Schema(type="string", default="10")
     *     ),
     *     @OA\Parameter(
     *         name="relations",
     *         in="query",
     *         description="the relations the requesting entity is asking for himself",
     *         required=false,
     *         @OA\Schema(type="string", default="creator")
     *     ),
     *     @OA\Response(
     *      @OA\MediaType(mediaType="application/json"),response="200",
     *      description="and structure of the response is json",
     *     ),
     * )
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function matches(Request $request);


    /**
     * @OA\Post(
     *     path="/api/getMatch",
     *     operationId="/getMatch",
     *     security={ {"bearer": {}} },
     *     tags={"matches section"},
     *     @OA\Parameter(
     *         name="id",
     *         in="query",
     *         description="the id of the corresponding match",
     *         required=false,
     *         @OA\Schema(type="string", default="1")
     *     ),
     *     @OA\Parameter(
     *         name="relations",
     *         in="query",
     *         description="the relations the requesting entity is asking for himself",
     *         required=false,
     *         @OA\Schema(type="string", default="creator")
     *     ),
     *     @OA\Response(
     *      @OA\MediaType(mediaType="application/json"),response="200",
     *      description="and structure of the response is json",
     *     ),
     * )
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function getMatch(Request $request);



    /**
     * @OA\Post(
     *     path="/api/createMatch",
     *     operationId="/createMatch",
     *     security={ {"bearer": {}} },
     *     tags={"matches section"},
     *     @OA\Parameter(
     *         name="game_id",
     *         in="query",
     *         description="the id of the game game user wants to create the match",
     *         required=true,
     *         @OA\Schema(type="string", default="1")
     *     ),
     *     @OA\Parameter(
     *         name="price",
     *         in="query",
     *         description="the price the user is going to set for the match",
     *         required=false,
     *         @OA\Schema(type="string", default="0")
     *     ),
     *     @OA\Parameter(
     *         name="user_account_id",
     *         in="query",
     *         description="the account id of the user which wants to play the match",
     *         required=true,
     *         @OA\Schema(type="string", default="4")
     *     ),
     *     @OA\Parameter(
     *         name="start_time",
     *         in="query",
     *         description="the time which the game is going to be started",
     *         required=false,
     *         @OA\Schema(type="string", default="2019-10-25 08:55:21")
     *     ),
     *     @OA\Parameter(
     *         name="max_players",
     *         in="query",
     *         description="maximum number of users for the match",
     *         required=false,
     *         @OA\Schema(type="string", default="1")
     *     ),
     *     @OA\Parameter(
     *         name="message",
     *         in="query",
     *         description="the message of the game admin",
     *         required=false,
     *         @OA\Schema(type="string", default="hik this is my match")
     *     ),
     *     @OA\Parameter(
     *         name="min_players",
     *         in="query",
     *         description="minimum number of users for the match",
     *         required=false,
     *         @OA\Schema(type="string", default="1")
     *     ),
     *     @OA\Response(
     *      @OA\MediaType(mediaType="application/json"),response="200",
     *      description="and structure of the response is json",
     *     ),
     * )
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function createMatch(CreateMatchRequest $request);

     




       /**
     * @OA\Post(
     *     path="/api/editMatch",
     *     operationId="/editMatch",
     *     security={ {"bearer": {}} },
     *     tags={"matches section"},
     *     @OA\Parameter(
     *         name="match_id",
     *         in="query",
     *         description="the id of the game game user wants to create the match",
     *         required=true,
     *         @OA\Schema(type="string", default="1")
     *     ),
     *     @OA\Parameter(
     *         name="price",
     *         in="query",
     *         description="the price the user is going to set for the match",
     *         required=false,
     *         @OA\Schema(type="string", default="0")
     *     ),
     *     @OA\Parameter(
     *         name="user_account_id",
     *         in="query",
     *         description="the account_id of the user which wants to play the match",
     *         required=false,
     *         @OA\Schema(type="string", default="4")
     *     ),
     *     @OA\Parameter(
     *         name="start_time",
     *         in="query",
     *         description="the time which the game is going to be started",
     *         required=false,
     *         @OA\Schema(type="string", default="2019-10-25 08:55:21")
     *     ),
     *     @OA\Parameter(
     *         name="max_players",
     *         in="query",
     *         description="maximum number of users for the match",
     *         required=false,
     *         @OA\Schema(type="string", default="1")
     *     ),
     *     @OA\Parameter(
     *         name="min_players",
     *         in="query",
     *         description="minimum number of users for the match",
     *         required=false,
     *         @OA\Schema(type="string", default="1")
     *     ),
     *     @OA\Parameter(
     *         name="message",
     *         in="query",
     *         description="the message of the game admin",
     *         required=false,
     *         @OA\Schema(type="string", default="hik this is my match")
     *     ),
     *     @OA\Response(
     *      @OA\MediaType(mediaType="application/json"),response="200",
     *      description="and structure of the response is json",
     *     ),
     * )
     *
     * @param Request $request
     *
     * @return mixed
     */  
    public function editMatch(EditMatchRequest $request);

    /**
     * @OA\Post(
     *     path="/api/increaseCredit",
     *     operationId="/increaseCredit",
     *     security={ {"bearer": {}} },
     *     tags={"matches section"},
     *     @OA\Parameter(
     *         name="customer_id",
     *         in="query",
     *         description="the customer_id ",
     *         required=false,
     *         @OA\Schema(type="string", default="1")
     *     ),
     *     @OA\Parameter(
     *         name="credit",
     *         in="query",
     *         description="the credit",
     *         required=false,
     *         @OA\Schema(type="string", default="5000")
     *     ),
     *     @OA\Response(
     *      @OA\MediaType(mediaType="application/json"),response="200",
     *      description="and structure of the response is json",
     *     ),
     * )
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function increaseCredit(Request $request);

    /**
     * @OA\Post(
     *     path="/api/decreaseCredit",
     *     operationId="/decreaseCredit",
     *     security={ {"bearer": {}} },
     *     tags={"matches section"},
     *     @OA\Parameter(
     *         name="customer_id",
     *         in="query",
     *         description="the customer_id ",
     *         required=false,
     *         @OA\Schema(type="string", default="1")
     *     ),
     *     @OA\Parameter(
     *         name="credit",
     *         in="query",
     *         description="the credit",
     *         required=false,
     *         @OA\Schema(type="string", default="5000")
     *     ),
     *     @OA\Response(
     *      @OA\MediaType(mediaType="application/json"),response="200",
     *      description="and structure of the response is json",
     *     ),
     * )
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function descreaseCredit(Request $request);


    /**
     * @OA\Post(
     *     path="/api/cancellMatch",
     *     operationId="/cancellMatch",
     *     security={ {"bearer": {}} },
     *     tags={"matches section"},
     *     @OA\Parameter(
     *         name="match_id",
     *         in="query",
     *         description="the match id that the user has already created ",
     *         required=true,
     *         @OA\Schema(type="string", default="1")
     *     ),
     *     @OA\Response(
     *      @OA\MediaType(mediaType="application/json"),response="200",
     *      description="will actually delete a match which user has created",
     *     ),
     * )
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function cancellMatch(Request $request);



    /**
   * @OA\Post(
   *     path="/api/leaveMatch",
   *     operationId="/leaveMatch",
     *     security={ {"bearer": {}} },
   *     tags={"matches section"},
   *     @OA\Parameter(
   *         name="match_id",
   *         in="query",
   *         description="the id of the match user wants to leave",
   *         required=true,
   *         @OA\Schema(type="string", default="1")
   *     ),
   *     @OA\Response(
   *      @OA\MediaType(mediaType="application/json"),response="200",
   *      description="this function will remove the user from the list of the match",
   *     ),
   * )
   *
   * @param Request $request
   *
   * @return mixed
   */ 
    public function userLeaveMatch(Request $request);



     /**
   * @OA\Post(
   *     path="/api/acceptMatchInvitiation",
   *     operationId="/acceptMatchInvitiation",
      *     security={ {"bearer": {}} },
   *     tags={"matches section"},
   *     @OA\Parameter(
   *         name="match_id",
   *         in="query",
   *         description="the id of the match user wants to leave",
   *         required=true,
   *         @OA\Schema(type="string", default="1")
   *     ),
   *     @OA\Parameter(
   *         name="invitation_id",
   *         in="query",
   *         description="the id of the invitation passed by the user",
   *         required=true,
   *         @OA\Schema(type="string", default="1")
   *     ),
   *     @OA\Parameter(
   *         name="userAccountId",
   *         in="query",
   *         description="the game id of the user for joining the game",
   *         required=true,
   *         @OA\Schema(type="string", default="1")
   *     ),
   *     @OA\Response(
   *      @OA\MediaType(mediaType="application/json"),response="200",
   *      description="will join the users using the invitation he was invited to the game",
   *     ),
   * )
   *
   * @param Request $request
   *
   * @return mixed
   */ 
    public function joinMatchByInvitations(Request $request);

   /**
   * @OA\Post(
   *     path="/api/inviteUserToMatch",
   *     operationId="/inviteUserToMatch",
    *     security={ {"bearer": {}} },
   *     tags={"matches section"},
   *     @OA\Parameter(
   *         name="match_id",
   *         in="query",
   *         description="the id of the match user wants to leave",
   *         required=true,
   *         @OA\Schema(type="string", default="1")
   *     ),
   *     @OA\Parameter(
   *         name="target_user_id",
   *         in="query",
   *         description="the id of the user that is meant to be invited",
   *         required=true,
   *         @OA\Schema(type="string", default="1")
   *     ),
   *     @OA\Response(
   *      @OA\MediaType(mediaType="application/json"),response="200",
   *      description="will join the users using the invitation he was invited to the game",
   *     ),
   * )
   *
   * @param Request $request
   *
   * @return mixed
   */ 
    public function inviteUserToMatch(Request $request);

    /**
   * @OA\Get(
   *     path="/api/userInvitations",
   *     operationId="/userInvitations",
     *     security={ {"bearer": {}} },
   *     tags={"matches section"},
   *     @OA\Response(
   *      @OA\MediaType(mediaType="application/json"),response="200",
   *      description="will return the invitations of the specific user ",
   *     ),
   * )
   *
   * @param Request $request
   *
   * @return mixed
   */ 
    public function userInvitations(Request $requst);

  

   /**
   * @OA\Get(
   *     path="/api/getUserInvitations",
   *     operationId="/getUserInvitations",
    *     security={ {"bearer": {}} },
   *     tags={"matches section"},
   *     @OA\Parameter(
   *         name="user_id",
   *         in="query",
   *         description="the id of the user we want to get its invitations",
   *         required=true,
   *         @OA\Schema(type="string", default="1")
   *     ),
   *     @OA\Response(
   *      @OA\MediaType(mediaType="application/json"),response="200",
   *      description="return the invitations for specific user ",
   *     ),
   * )
   *
   * @param Request $request
   *
   * @return mixed
   */ 
    public function getUserInvitations(Request $request);



   /**
   * @OA\Post(
   *     path="/api/addGameAccount",
   *     operationId="/addGameAccount",
    *     security={ {"bearer": {}} },
   *     tags={"games section"},
   *     @OA\Parameter(
   *         name="game_id",
   *         in="query",
   *         description="the game id the user wants to add the account for it ",
   *         required=true,
   *         @OA\Schema(type="string", default="1")
   *     ),
   *     @OA\Parameter(
   *         name="user_account_id",
   *         in="query",
   *         description="the user account id in the specific game ",
   *         required=true,
   *         @OA\Schema(type="string", default="1")
   *     ),
   *     @OA\Response(
   *      @OA\MediaType(mediaType="application/json"),response="200",
   *      description="the response will be user account info if added and an error if it was not done",
   *     ),
   * )
   *
   * @param Request $request
   *
   * @return mixed
   */
    public function addGameAccount(Request $request);
    



   /**
   * @OA\Post(
   *     path="/api/kickMatchSide",
   *     operationId="/kickMatchSide",
    *     security={ {"bearer": {}} },
   *     tags={"matches section"},
   *     @OA\Parameter(
   *         name="match_id",
   *         in="query",
   *         description="the id of the match",
   *         required=true,
   *         @OA\Schema(type="string", default="1")
   *     ),
   *     @OA\Response(
   *      @OA\MediaType(mediaType="application/json"),response="200",
   *      description="this function will kick the guest side of the match by the match owner",
   *     ),
   * )
   *
   * @param Request $request
   *
   * @return mixed
   */
    public function kickMatchSide(Request $request);




    /**
     * @OA\Post(
     *     path="/api/joinMatch",
     *     operationId="/joinMatch",
     *     security={ {"bearer": {}} },
     *     tags={"matches section"},
     *     @OA\Parameter(
     *         name="match_id",
     *         in="query",
     *         description="the id of the match",
     *         required=true,
     *         @OA\Schema(type="string", default="1")
     *     ),
     *     @OA\Parameter(
     *         name="side",
     *         in="query",
     *         description="the side of which user wants to join(the default side is 1",
     *         required=true,
     *         @OA\Schema(type="string", default="1")
     *     ),
     *     @OA\Parameter(
     *         name="userAccountId",
     *         in="query",
     *         description="the account id of the game which the user wants to join",
     *         required=true,
     *         @OA\Schema(type="string", default="elomir")
     *     ),
     *     @OA\Response(
     *      @OA\MediaType(mediaType="application/json"),response="200",
     *      description="for joining the game by the user id and will be the side admin of the match",
     *     ),
     * )
     *
     * @param Request $request
     *
     * @return mixed
    */
    public function joinMatchAsSideAdmin(JoinMatchRequest $reqeust);



    /**
     * @OA\Post(
     *     path="/api/getUserMatches",
     *     operationId="/getUserMatches",
     *     security={ {"bearer": {}} },
     *     tags={"matches section"},
     *     @OA\Parameter(
     *         name="match_id",
     *         in="query",
     *         description="the id of the match",
     *         required=false,
     *         @OA\Schema(type="string", default="1")
     *     ),
     *     @OA\Parameter(
     *         name="relations",
     *         in="query",
     *         description="relations of the matches like admin,users",
     *         required=false,
     *         @OA\Schema(type="string", default="users")
     *     ),
     *     @OA\Parameter(
     *         name="time",
     *         in="query",
     *         description="the date the matche was created ",
     *         required=false,
     *         @OA\Schema(type="string", default="")
     *     ),
     *     @OA\Parameter(
     *         name="start_time",
     *         in="query",
     *         description="the time variation in which the user wants to get the results",
     *         required=false,
     *         @OA\Schema(type="string", default="")
     *     ),
     *     @OA\Response(
     *      @OA\MediaType(mediaType="application/json"),response="200",
     *      description="for returning the matches list of specific user with related parameters",
     *     ),
     * )
     *
     * @param Request $request
     *
     * @return mixed
    */
    public function getUserMatches(Request $request);


    /**
     * @OA\Post(
     *     path="/api/kickMatchPlayer",
     *     operationId="/kickMatchPlayer",
     *     security={ {"bearer": {}} },
     *     tags={"matches section"},
     *     @OA\Parameter(
     *         name="match_id",
     *         in="query",
     *         description="the id of the match",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="user_id",
     *         in="query",
     *         description="the id of the user",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *      @OA\MediaType(mediaType="application/json"),response="200",
     *      description="kicking the user of the match",
     *     ),
     * )
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function kickMatchUser(Request $request);
}