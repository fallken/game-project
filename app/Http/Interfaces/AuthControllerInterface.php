<?php

namespace App\Http\Interfaces;

use Illuminate\Http\Request;

interface AuthControllerInterface
{
    /**
     * @OA\Post(
     *     path="/api/register",
     *     operationId="/register",
     *     tags={"user authentications"},
     *     @OA\Parameter(
     *         name="name",
     *         in="query",
     *         description="Name of the user",
     *         required=true,
     *         @OA\Schema(type="string", default="mammad")
     *     ),
     *     @OA\Parameter(
     *         name="email",
     *         in="query",
     *         description="email of the user ",
     *         required=true,
     *         @OA\Schema(type="string", default="arsalani@outlook.com")
     *     ),
     *     @OA\Parameter(
     *         name="password",
     *         in="query",
     *         description="password of the user ",
     *         required=true,
     *         @OA\Schema(type="string", default="1234567")
     *     ),
     *     @OA\Response(
     *      @OA\MediaType(mediaType="application/json"),response="200",
     *      description="and structure of the response is json",
     *     ),
     * )
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function register(Request $request);

    /**
     * @OA\Post(
     *     path="/api/login",
     *     operationId="/login",
     *     tags={"user authentications"},
     *     @OA\Parameter(
     *         name="email",
     *         in="query",
     *         description="email of the user ",
     *         required=true,
     *         @OA\Schema(type="string", default="arsalani@outlook.com")
     *     ),
     *     @OA\Parameter(
     *         name="password",
     *         in="query",
     *         description="password of the user ",
     *         required=true,
     *         @OA\Schema(type="string", default="1234567")
     *     ),
     *     @OA\Response(
     *      @OA\MediaType(mediaType="application/json"),response="200",
     *      description="and structure of the response is json",
     *     ),
     * )
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function login(Request $request);

    /**
     * @OA\Get(
     *     path="/api/user",
     *     operationId="/user",
     *     security={ {"bearer": {}} },
     *     tags={"user authentications"},
     *     @OA\Response(
     *      @OA\MediaType(mediaType="application/json"),response="200",
     *      description="will return the current authenticated user ",
     *     ),
     * )
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function user(Request $request);


    /**
     * @OA\Get(
     *     path="/api/logout",
     *     operationId="/logout",
     *     security={ {"bearer": {}} },
     *     tags={"user authentications"},
     *     @OA\Response(
     *      @OA\MediaType(mediaType="application/json"),response="200",
     *      description="and structure of the response is json",
     *     ),
     * )
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function logout(Request $request);
}