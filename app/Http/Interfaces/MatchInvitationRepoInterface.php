<?php

namespace App\Http\Interfaces;

interface MatchInvitationRepoInterface{
    public function sendMatchInvition($user_id,$target_user_id,$match_id,$side);
}