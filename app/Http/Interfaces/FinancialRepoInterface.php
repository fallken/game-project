<?php

namespace App\Http\Interfaces;

interface FinancialRepoInterface{
    #add credit to custome like credit of winning a match
    public function addCreditToCustomer($customer_id=null,$credit,$user=null,$transaction_data=null);

    #reducing the credit from the customer
    public function reduceCreditFromCustomer($customer_id,$credit);

    #reduce the share of the application
    public function getApplicationShare($match_id=null,$match=null,$percent=0.05);

    #add admin credit like after each match he will be given the price
    public function addAdminCredit($admin_id=null,$admin=null,$credit,$transaction_data=null);

    #reduce admin credit 
    public function reduceAdminCredit($admin_id=null,$admin=null,$credit,$transaction_data=null);

    #create transactions regarding the matches and other stuff
    public function createTransaction($data);


    public function calculateAdminPrice($match_id=null,$match=null,$percent=0.05);
}