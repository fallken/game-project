<?php

namespace App\Http\Interfaces;

use App\Http\Requests\API\SetCategoryRequest;

use Illuminate\Http\Request;

interface CategoryControllerInterface
{
    /**
     * @OA\Post(
     *     path="/api/categories",
     *     operationId="/categories/storeOrUpdate",
     *     security={ {"bearer": {}} },
     *     tags={"Categoreis"},
     *     @OA\Parameter(
     *         name="name",
     *         in="query",
     *         description="Name of the category",
     *         required=true,
     *         @OA\Schema(type="string", default="")
     *     ),
     *     @OA\Parameter(
     *         name="image",
     *         in="query",
     *         description="Image file of category.",
     *         required=false,
     *         @OA\Schema(type="string", default="")
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="query",
     *         description="Id of corresponding category for update.",
     *         required=false,
     *         @OA\Schema(type="string", default="")
     *     ),
     *     @OA\Response(
     *      @OA\MediaType(mediaType="application/json"),response="200",
     *      description="Will return id, name, image of created category.",
     *     ),
     * )
     *
     * Store category
     *
     * @param SetCategoryRequest $request
     *
     * @return mixed
     */
    public function storeOrUpdate(SetCategoryRequest $request);

    /**
     * @OA\GET(
     *     path="/api/categories",
     *     operationId="/categories",
     *     security={ {"bearer": {}} },
     *     tags={"Categoreis"},
     *     @OA\Response(
     *      @OA\MediaType(mediaType="application/json"),response="200",
     *      description="Will return array of id, name, image of categories.",
     *     ),
     * )
     *
     * Get categories.
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function index(Request $request);

    /**
     * @OA\GET(
     *     path="/api/categories/{categoryId}",
     *     operationId="/categories/Show",
     *     security={ {"bearer": {}} },
     *     tags={"Categoreis"},
     *     @OA\Parameter(
     *         name="categoryId",
     *         in="path",
     *         description="Category id",
     *         required=true,
     *         @OA\Schema(type="string", default="")
     *     ),
     *     @OA\Response(
     *      @OA\MediaType(mediaType="application/json"),response="200",
     *      description="Will return id, name, image of category.",
     *     ),
     * )
     *
     * Get single category
     *
     * @param $categoryId
     *
     * @return mixed
     */
    public function show($categoryId);
}