<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use App\Http\Controllers\Controller;
use App\Repositories\CategoriesRepository;
use App\Http\Requests\API\SetCategoryRequest;
use App\Http\Interfaces\CategoryControllerInterface;

class CategoryController extends Controller implements CategoryControllerInterface
{
    /**
     * @var CategoriesRepository
     */
    private $categoriesRepository;

    /**
     * CategoryControllerController constructor.
     */
    public function __construct()
    {
        $this->categoriesRepository = new CategoriesRepository();
    }

    /**
     * Store category.
     *
     * @param SetCategoryRequest $request
     *
     * @return mixed
     * @throws \App\Exceptions\BadUploadFileException
     */
    public function storeOrUpdate(SetCategoryRequest $request)
    {
        return $this->categoriesRepository->storeOrUpdate(
            $request->input('name'),
            $request->file('image'),
            $request->input('id')
        );
    }

    /**
     * Get categories.
     *  
     * @param Request $request
     *
     * @return mixed
     */
    public function index(Request $request)
    {   
        Log::info('working like charm');
        #will return all the game categories for the application
        return ['results' => $this->categoriesRepository->returnAllCategories()];
    }

    /**
     * @param $categoryId
     *
     * @return mixed|void
     */
    public function show($categoryId)
    {
        $result = $this->categoriesRepository->fetchById($categoryId);

        if (!$result) {
            abort(404);
        }

        return ['result' => $result];
    }
}
