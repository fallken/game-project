<?php

namespace App\Http\Controllers\Api;

use App\Events\SocketMessage;
use App\Models\User;
use App\Traits\Messaging;
use App\Traits\Utils;
use App\Models\Message;
use App\Traits\General;
use App\Models\GameAdmin;
use App\Models\UserFriend;
use App\Jobs\NewFriendAdded;
use Illuminate\Http\Request;
use App\Jobs\SendFriendRequest;
use Illuminate\Support\Facades\App;
use App\Http\Controllers\Controller;
use App\Http\Interfaces\UserControllerInterface;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redis;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserController extends Controller implements UserControllerInterface
{
    use General,Utils,Messaging;
    public function __construct(){
        
    }
    public function searchUsers(Request $request){
      // if($request->game_account == true){
      //  $users = UserGame::where('account_user_name', 'like', '%' . $request->user_name . '%')->
      //  orWhere('account_id', 'like', '%' . $request->user_name . '%')->take(12)->get();
      // }else{
       $users = User::select('name','email','avatar')->where('name', 'like', '%' . $request->user_name . '%')->
       orWhere('email', 'like', '%' . $request->user_name . '%')->take(12)->get();
      // }
      return response()->json(['message'=>$users],200);
    }
    #return the profile details of the user in the final stage . 
     public function getUserProfile(Request $request){
       $user = $request->user();
       $data = collect([
          'user'=>$user,
          'matches'=>$user->matches()->orderBy('created_at','desc')->take(12)->get(),
          'friends'=>$user->friends(),
          'games'=>$user->games,
          'game_admin'=>GameAdmin::where('user_id',$user->id)->with(['game'])->get()
       ]);
       return response()->json(['message'=>$data],200);
    }
    //will send add friend request to specific user 
    public function addFriend(Request $request){
      $user = $request->user();
      if($request->friend_id == $user->id){
        return response()->json(['message'=>__('user.cant_add_friend_himself')],403);
      }
      if($this->userIsFriend($user->id,$request->friend_id)){#check if the user is already friend with the user or not
        return response()->json(['message'=>__('user.user_already_friend')],403);
      }
      if($this->friendRequestAlreadyExists($user->id,$request->friend_id)){#check if the user has already sent the sepcified user the friend request and the status is still pending  
        return response()->json(['message'=>__('user.friend_request_exists')],403);
      }

      #end of dispatching user 
      $result = UserFriend::create([
        'user_id'=>$request->user()->id,
        'friend_id'=>$request->friend_id,
        'status'=>'pending',
      ]);
      //call the job (for sending notificaiton to the user .)
        #send the friend request to redis or log or any other thing .
        event(new SocketMessage('message',[
            'from'=>$request->user()->name,//could be null.
            'name'=>'friend_request',
            'message'=>'hi add me as your friend',
            'details'=>$result,
            'all'=>false,
            'to'=>$request->friend_id
        ]));

      if(!$result) return response()->json(['message'=>__('user.problem_sending_request')],500);
      return response()->json(['message'=>__('user.friend_request_success')],200);
    }
    //will remove a friend from its list 
    public function removeFriend(Request $request){
        
    }
    //will accept friend request sent for it 
    public function setFriendReuqestStatus(Request $request){
        $user = $request->user();
        $friend_request = UserFriend::find($request->request_id);
        if(!$friend_request){
          return response()->json(['message'=>__('user.friend_request_not_exist')],404);
        }
        if($friend_request->friend_id != $user->id){
          return response()->json(['message'=>__('user.user_not_own_request')],403);
        }
        if($user->friends()->count() >= $user->max_friend_no){
          return response()->json(['message'=>__('user.maximum_friend_limit')],403);
        }
        if($friend_request->status !== 'pending'){
          return response()->json(['message'=>__('user.request_unavailable')],404);
        }

        $result =  $friend_request->update([
          'status'=>$request->status
        ]);
        //dispatching for sending the hosting user the notification that someone  new is his friend .
        if($request->status == 'accepted'){
          #creating a mutual friendship connection (im doing this cause i want to make a subscribe and follower structure in the future so it will become handy)
            $friend_request->update([
                'accepted_at'=>date('Y-m-d H:i:s',time())
            ]);
            $newFriendResult = UserFriend::create([
            'user_id'=>$request->user()->id,
            'friend_id'=>$friend_request->user_id,
            'status'=>'accepted',
            'accepted_at'=>date('Y-m-d H:i:s',time())
             ]);
            foreach ([$request->user()->id,$friend_request->user_id] as $userId){
                event(new SocketMessage('message',[
                    'from'=>$userId,//could be null.
                    'name'=>config('socketTypes.new_friend'),
                    'message'=>'you have a new friend',
                    'details'=>[
                        'friends'=>$this->getFriendIds($userId),
                        'result'=>$newFriendResult
                    ],
                    'all'=>false,
                    'to'=>$userId
                ]));
            }

        }
        if(!$result){
          return response()->json(['message'=>__('user.problem_changin_friend_request_status')],500);
        }

        return response()->json(['message'=>__('user.request_status_change_success')],200);
    }
    //will give user the friend requests either the sent ones or even the recieved ones . 
    public function friendRequests(Request $request){
//        exit(json_encode(JWTAuth::parseToken()->getPayload()));
        $requests = $this->getFriendRequests($request->user()->id);
        return [
          'requests'=>$requests,
        ];
    }
    //a fnction for a user for sending messages to other users .
    public function sendMessageToUser(Request $request){
        //check if the user is his friend . check if the user is not banned . check if the user is not blocked by the other side.
        if(!$this->userIsFriend($request->user()->id,$request->target_user_id)){
            return response()->json(['message'=>'user is not your friend'],403);
        }
        $message = [
            'user_id'=>$request->user()->id,
            'user_name'=>$request->user()->name,
            'target_user_id'=>$request->target_user_id,
            'text'=>$request->message_body,
            'type'=>'talk'
        ];
       $result =  $this->sendMessage($message);
       if($result){
           return response()->json(['message'=>$result],200);
       }
       return response()->json(['message'=>'problem ssending the message to user'],500);
    }

    public function userMessages(Request $request){
        $offset = Input::get('offset')??0;
        $messages = $request->user()->messages($request->input($offset));//should check it.
        return response()->json(['message'=>$messages],200);
    }
}
