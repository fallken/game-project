<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Traits\General;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class SocialController extends Controller
{
    use General;

    public function redirect($provider){
        return Socialite::driver($provider)->stateless()->redirect()->getTargetUrl();
    }

    public function Callback($provider){
        $userSocial =   Socialite::driver($provider)->stateless()->user();

        $user   =   User::where(['email' => $userSocial->getEmail()])->first();
        if($user){
            Auth::login($user);
            $user = Auth::user();
            $token =  $user->createToken('Personal Access Token')->accessToken;
            $cookie = $this->getCookieDetails($token);
            $response =new \Illuminate\Http\Response([
                'logged_in_user' => $user,
                'token' => $token,
            ]);
            $response->cookie($cookie['name'], $cookie['value'], $cookie['minutes'], $cookie['path'], $cookie['domain'], $cookie['secure'], $cookie['httponly'], $cookie['samesite']);
            return $response;
        }else{
            $user = User::create([
                'name'          => $userSocial->getName(),
                'email'         => $userSocial->getEmail(),
                'avatar'         => $userSocial->getAvatar(),
                'provider_id'   => $userSocial->getId(),
                'provider'      => $provider,
        ]);
        return redirect()->route('home');
        }
    }




   
}
