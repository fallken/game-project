<?php

namespace App\Http\Controllers\Api;

use App\Models\Match;
use App\Traits\Utils;
use App\Models\GameAdmin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Interfaces\MatchesInterface;
use App\Jobs\AdminGameRegistrationRequest;
use App\Http\Interfaces\GamesRepoInterface;
use App\Http\Interfaces\FinancialRepoInterface;
use App\Http\Interfaces\GameAdminControllerInterface;

class GameAdminController extends Controller implements GameAdminControllerInterface
{
    use Utils;
    public $matches;

    public function __construct(MatchesInterface $matches, FinancialRepoInterface $financial, GamesRepoInterface $gameRepo)
    {
        $this->matches = $matches;
    }

    #join the admin to the match
    public function adminJoinMatch(Request $request)
    {
        if ($this->matchHasAdmin($request->match_id)) {
            return response()->json(['message' => __('gameAdmin.match_has_admin')], 403);
        }
        if (!$admin = $this->checkIsAdmin($request->user()->id, $request->admin_id)) {
            return response()->json(['message' => __('gameAdmin.user_not_game_admin')], 403);
        }
        $match = Match::find($request->match_id);
        if ($admin->game_id != $match->game_id) {
            return response()->json(['message' => __('gameAdmin.account_not_for_this_game')], 403);
        }
        if ($this->checkUserAlreadyInMatch($request->user(), $request->match_id)) {
            return response()->json(['message' => __('gameAdmin.admin_already_a_player')], 403);
        }
        if (in_array($match->status, ['ongoing', 'finished', 'stopped', 'failed'])) {
            return response()->json(['message' => __('gameAdmin.match_not_accessable')], 403);
        }
        #join the admin to the match
        $result = $this->matches->joinAdminToMatch($request->match_id, $admin->id);
        if(!$result){
            return response()->json(['message'=>__('gameAdmin.problem_joining_match')],200);
        }

        return response()->json(['message'=>__('gameAdmin.join_match_success')],200);
    }

    #once the admin wants to leave the match
    public function adminLeaveMatch(Request $request)
    {
        if (!$admin_id = $this->checkIsAdmin($request->user()->id)) {
            return response()->json(['message' => __('gameAdmin.user_not_game_admin')], 403);
        }
        if (!$this->checkIsMatchGameAdmin($request->match_id, $admin_id)) {
            return response()->json(['message' => __('gameAdmin.not_this_game_admin')], 401);
        }
        $match = Match::find($request->match_id);
        if (in_array($match->status, ['ongoing', 'finished', 'stopped', 'failed'])) {
            return response()->json(['message' => __('gameAdmin.admin_cant_leave_cause_statuts')], 403);
        }
        #leave the admin from the match
        $result = $this->matches->removeAdminFromTheMatch($request->match_id, $admin_id);
        if ($result) {
            return response()->json(['message' => __('gameAdmin.admin_remove_successfull')], 200);
        }
        return response()->json(['message' => __('gameAdmin.problem_removing_admin')], 500);
    }

    #start the match with enough validations about the admin
    public function adminStartMatch(Request $request)
    {
        $match = Match::find($request->match_id);
        if (!$this->checkIsMatchGameAdmin($request->match_id, $request->admin_id)) {
            return response()->json(['message' => __('gameAdmin.not_this_game_admin')], 401);
        }
        if (in_array($match->status, ['ongoing', 'finished', 'stopped', 'failed'])) {
            return response()->json(['message' => __('gameAdmin.match_status_cant_be_changed')], 403);
        }
        $result = $this->matches->changeMatchStatus(null, $match, config('matches.status.2'));#set matches to ongoing
        if (!$result) {
            return response()->json(['message' => __('gameAdmin.problem_starting_match')], 500);
        }
        return response()->json(['message' => __('gameAdmin.start_match_success')], 200);
    }

    #very important function we have here.  
    public function endMatch(Request $request)
    {
        $match = Match::find($request->match_id);
        if (!$this->checkIsMatchGameAdmin($request->match_id, $request->admin_id)) {
            return response()->json(['message' => __('gameAdmin.not_this_game_admin')], 401);
        }
        if (in_array($match->status, ['finished', 'stopped', 'failed'])) {
            return response()->json(['message' => __('gameAdmin.match_already_over')], 403);
        }
        $result = $this->matches->setMatchFinishStatus($request->match_id, $request->winner_side, 'finished', $request->admin_id);#set matches to ongoing
        if ($result) {
            return response()->json(['message' => __('gameAdmin.match_finished_shared_profit')], 200);
        }
        return response()->json(['message' => __('gameAdmin.problem_ending_match')], 500);
    }

    #change the status of the match to failed or stopped 
    public function changeMatchStatus(Request $request)
    {
        $result = '';
        if ($result) {
            return response()->json(['message' => __('gameAdmin.match_finished_shared_profit')], 200);
        }
        return response()->json(['message' => __('gameAdmin.problem_ending_match')], 500);
    }

    #a bit controversial but it will be fine i guess :)
    public function pauseMatch(Request $request)
    {
        if (!$this->checkIsMatchGameAdmin($request->match_id, $request->admin_id)) {
            return response()->json(['message' => __('gameAdmin.not_this_game_admin')], 401);
        }
        $match = Match::find($request->match_id);
        if (!in_array($match->status, ['ongoing'])) {#if the match is not on the ongoing state then it cannot be paused
            return response()->json(['message' => __('gameAdmin.match_not_running_to_pause')], 403);
        }
        $this->matches->changeMatchStatus($request->match_id, config('matches.status.2'));#set matches to ongoing
    }

    #get the user request for registering as game admin . 
    public function requestForGameAdmin(Request $request)
    {

        //check if the user is already an admin for the game
        $user = $request->user();
        if (!$this->userHasGame($user->id, $request->game_id, $request->account_id)) {
            return response()->json(['message' => __('gameAdmin.user_not_own_game_account')], 403);
        }
        if ($this->isGameAdmin($user->id, $request->game_id)) {
            return response()->json(['message' => __('gameAdmin.only_one_admin_for_each_game')], 403);
        }
        $result = GameAdmin::create([
            'user_id' => $user->id,
            'game_id' => $request->game_id,
            'account_id' => $request->account_id
        ]);
        if (!$result) {
            return response()->json(['message' => __('gameAdmin.problem_registring_admin_to_game')], 500);
        }
        dispatch(new AdminGameRegistrationRequest);
        return response()->json(['message' => __('gameAdmin.add_account_success_wait_verification')], 200);
    }

    public function kickMatchUser(Request $request)
    {
        $match = Match::find($request->match_id);
       
        if (!$this->checkIsMatchGameAdmin($request->match_id, $request->admin_id)) {
            return response()->json(['message' => __('gameAdmin.not_this_game_admin')], 401);
        }
        if (in_array($match->status, ['ongoing', 'finished', 'stopped', 'failed'])) {
            return response()->json(['message' => __('gameAdmin.match_status_cant_be_changed')], 403);
        }
        if ($this->checkIsGameSideAdmin($request->match_id, null, $request->user_id)) {
            $user_side = $this->getSide($request->user_id, $request->match_id);
            if ($user_side == 0) {//if the user side is the match main side then just return an error(cause the game admin is not allowed to do like this)
                return response()->json(['message' => __('gameAdmin.cant_kick_match_owner')], 200);
            } else {
                $result = $this->matches->kickMatchSide($request->match_id, $user_side);//user side is actually 1 for the guest side .
                return response()->json(['message' => __('gameAdmin.side_kick_success')]);
            }
        }
        $result = $this->matches->deleteUserFromMatch($request->match_id, $request->user_id);
        if ($result)
            return response()->json(['message' => __('match.user_leave_success')], 200);
        else
            return response()->json(['message' => __('match.problem_deleting_user_match')], 500);
    }
}
