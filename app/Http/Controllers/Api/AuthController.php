<?php

namespace App\Http\Controllers\Api;

use App\Events\SocketMessage;
use App\Models\User;
use App\Traits\General;
use App\Traits\Utils;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Cookie;
use App\Mail\SendUserVerificationToken;
use App\Http\Interfaces\MailingInterface;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Validator;
use App\Http\Interfaces\AuthControllerInterface;

class AuthController extends Controller implements AuthControllerInterface
{
    use General,Utils;

    
    public $mailingRepo;
    public function __construct(MailingInterface $mailing)
    {
        $this->mailingRepo = $mailing;
    }

    /**
     * Register user.
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);

        if ($validator->fails()) {
            return response(['errors' => $validator->errors()->toArray()], 422);
        }

        $request['password'] = Hash::make($request->input('password'));

        $mailVerificationToken = \Illuminate\Support\Str::random(32);
        $inputs = $request->all(['name', 'email', 'password']);
        $inputs['email_verification_token'] = $mailVerificationToken;
        $user = User::create($inputs);

//        $token = $user->createToken('Laravel Password Grant Client')->accessToken;


        Mail::to($user->email)->send(new SendUserVerificationToken($user->email,$mailVerificationToken));

        return response([
            'status' => 'success',
//            'token' => $token ,
            'user'=>$user
        ]);
    }

    /**
     * Login user.
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);
        $credentials = ['email'=>$request->email,'password'=>$request->password,'status'=>1 ];
        if ($token = auth('api')->attempt($credentials)) {
            $user = auth('api')->user();
            $cookie = $this->getCookieDetails($token);
            $response =new \Illuminate\Http\Response([
                'logged_in_user' => $user,
                'token' => $token,
            ]);
            $response->cookie($cookie['name'], $cookie['value'], $cookie['minutes'], $cookie['path'], $cookie['domain'], $cookie['secure'], $cookie['httponly'], $cookie['samesite']);
            //calling hi is online
            $friends = $this->getFriendIds($user->id);
            if(count($friends) > 0){
                event(new SocketMessage('friend_online',[
                    'from'=>$user->name,//could be null.
                    'name'=>'friend_online',
                    'message'=>'friend is online',
                    'details'=>[$user],
                    'all'=>false,
                    'to'=>$friends
                ]));
            }
            return $response;
        } else {
            return response()->json(
                ['error' => 'invalid-credentials']
                , 422);
        }
    }

    /**
     * Get current user.
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function user(Request $request)
    {
        return $request->user();
    }

    /**
     * Logout logged user.
     *$
     * @param Request $request
     *
     * @return mixed
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        $cookie = Cookie::forget('_token');
        return response()->json([
            'message' => 'successful-logout'
        ])->withCookie($cookie);
    }

    /**
     * @param $token
     * @return \Illuminate\Http\JsonResponse
     */
    public function verifyEmail($token)
    {
        $user = User::where('email_verification_token',$token)->first();

        if (!$user):
            return response()->json([
                'message'=>__('UserNotFound')
            ],404);
        endif;
        $update_query = [
            'status'=>1,//set status to active
            'email_verification_token'=>null,
            'email_verified_at'=>date('Y-m-d H:i:s')
        ];
        $user->update($update_query);

        return response()->json([
            'message'=>__('user has been authenticated successfully-- you can log in now ')
        ],200);
    }




}
