<?php

namespace App\Http\Controllers\Api;

use App\Events\SocketMessage;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Match;
use App\Traits\Utils;
use App\Traits\General;
use App\Models\UserGame;
use Illuminate\Http\Request;
use App\Models\MatchInvitation;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Interfaces\MatchesInterface;
use App\Http\Interfaces\GamesRepoInterface;
use App\Http\Requests\API\EditMatchRequest;
use App\Http\Requests\Api\JoinMatchRequest;
use App\Http\Requests\API\CreateMatchRequest;
use App\Http\Interfaces\FinancialRepoInterface;
use App\Http\Interfaces\MatchControllerInterface;
use App\Http\Interfaces\MatchInvitationRepoInterface;
use Tymon\JWTAuth\Facades\JWTAuth;

class MatchController extends Controller implements MatchControllerInterface
{
    use General,Utils;
    public $matches,$invitation,$financial,$gameRepo;

    public function __construct(MatchesInterface $matches,MatchInvitationRepoInterface $matchInvitation,
    FinancialRepoInterface $financial,GamesRepoInterface $gameRepo)
    {
        $this->matches = $matches;
        $this->invitation = $matchInvitation;
        $this->financial = $financial;
        $this->gameRepo = $gameRepo;
    }

    public function testMatches()
    {
      return response()->json(($this->checkSideEmpty(3,1)?'true':'false'));
      return $this->invitation->sendMatchInvition(1,2,1,1);
      return $this->userHasMatch(2,Auth::user()->id)?'it has ':'it has not';
      return $this->userHasAnyMatch(Auth::user()->id)?'it has ':'it has not';
      return $this->userHasAnyMatch(Auth::user()->id)?'it has ':'it has not';
    }

    
    public function getMatch(Request $request)
    {
       $relations = $request->has('relations')?$this->getRelations($request->relations):null;
       $match = Match::with($relations)->find($request->id);
       return response()->json($match);
    }
    
    public function getUserMatches(Request $request){
// then either of
        event(new SocketMessage('matchjoiner',
            [
                'from'=>$request->user()->name,//could be null.
                'name'=>'userjoin',
                'message'=>'hi another message for you my friend ',
                'details'=>'another user info',
                'all'=>false,
                'to'=>$request->user()->id
            ]
        ));
//        $this->pushEventIntoRedis('message',['message'=>'hi this is some information and collection info']);
       $user = $request->user_id?User::find($request->user_id):$request->user();
       $relations = $request->has('relations')?$this->getRelations($request->relations):null;
       $matches = $user->matches()->skip($request->skip??0)->take($request->take??10);
       $matches = !is_null($relations)?$matches->with($relations):$matches;
       $matches = $request->has('match_id')?$matches->where('match_id',$request->match_id):$matches;
       $matches = $request->has('time')?$matches->where('matches.created_at', 'like', '%' . $request->time . '%'):$matches;
       $matches = $request->has('start_time')?$matches->where('start_time', 'like', '%' . $request->start_time . '%'):$matches;
       return response()->json(['message'=>$matches->get()],200);
    }



    
    public function matches(Request $request){
       $relations = $request->has('relations')?$this->getRelations($request->relations):null;
       $matches = $this->matches->getMatches($request->skip,$request->take,$relations,null,$request->game_id??null);
       return response()->json($matches);
    }
    
    public function createMatch(CreateMatchRequest $request){
       $user = $request->user();
       #check if the user has already set up a corresponding game account or not 
       if(!$this->userHasGame($user->id,$request->game_id,$request->user_account_id)){
            return response()->json(['message'=>__('match.no_game_account')],403);
       }
       #checking if the user already has the value with him or hersel or not . 
       if($user->wallet < $this->changeCurrency($request->price,$request->currency,$user->currency)){
            return response()->json(['message'=>__('match.not_enough_credit')],403);
       }
       #chceking the user input start time 
       if($request->has('start_time')){
          $date = new Carbon;
          if($date > $request->start_time){
            return response()->json(['message'=>__('match.time_set_future')],200);
          }
       }
      $result = DB::transaction(function() use ($request,$user){
           //creating a match
        $match_data  = [
          'game_id'=>$request->game_id,
          'creator_id'=>$user->id,
          'host_user_id'=>$user->id,
          'price'=>$request->price??0,
          'joined_users'=>0,//it will be augumented soon
          'message'=>$request->message,
          'start_time'=>$request->start_time??null,//the time format will be set to epoch format
         //'status'=>$request->status,
          'max_players'=>$request->max_players??null,
          'min_players'=>$request->min_players??null,
        ];
        $match = Match::create($match_data);
        #reduct credit frmo user account
        $transaction_data=[
                'user_id'=>$user->id,
                'game_id'=>$match->game_id,
                'match_id'=>$match->id,
                'amount'=>$match->price,
                'type'=>config('matches.transactionTypes.6'),
                'desc'=>"user payment for creating match $match->id as creator"
            ];
        $this->financial->reduceCreditFromCustomer(null,$match->price,$user,$transaction_data);
        #once the match is created the user should now be on the list of the user-matches list a
        $this->matches->joinUserToMatches($user->id,$match->id,0,$request->user_account_id,true);
        if($match) return response()->json(['message'=>$match],200);
        #in case of failure
        return response()->json(['message'=>__('match.problem_creating_match')],500);
      });
       
      return $result;       
    }

    public function increaseCredit(Request $request){
      $result = $this->financial->addCreditToCustomer($request->customer_id,$request->credit);
      #add transaction
      return $result;
    }

    public function descreaseCredit(Request $request){
      $result = $this->financial->reduceCreditFromCustomer($request->customer_id,$request->credit);
      #add transaction
      return $result;
    }

    #add a game account for the user . 
    public function addGameAccount(Request $request){
         $user = $request->user();
         if(!$this->checkGameExists($request->game_id)){
            return response()->json(['message'=>__('match.game_not_exist')],200);
         }
         #check if user already has the account or not . 
         if($this->userHasGame($user->id,$request->game_id,$request->user_account_id)){
            return response()->json(['message'=>__('match.user_similar_account')],403);
         }
         #check if other users already have this account or not .
         if($this->accountExists($request->user_account_id,$request->game_id)){
            return response()->json(['message'=>__('match.account_in_use')],401);
         }
         #get the account information from the user.
         $user_account = $this->getAccountFromApi($request->user_account_id , $request->game_id);
         if($user_account === false){
            return response()->json(['message'=>__('match.param_not_valid')],404);
         }
         $account_data = [
            'user_id'=>$user->id,
            'account_id'=>$request->user_account_id,
            'game_id'=>$request->game_id,
            'account_user_name'=>$request->user_account_id,
            'account_details'=>json_encode($user_account),
            'account_level'=>$user_account->account_level??null,#should be same on all of the api sysstems
         ];
         $addAccontResult = $this->gameRepo->addGameAccount($account_data);

         if($addAccontResult){
            return response()->json(['message'=>$addAccontResult],200);
         }else{
            return response()->json(['message'=>__('match.problem_adding_account')],500);
         }
    }
     #will edit the match created by the user 
    public function editMatch(EditMatchRequest $request){
       $user = $request->user();
      #check if the matche exists
      if(!$match = $this->checkMatchExists($request->match_id,true)){
         return response()->json(['message'=>__('match.match_not_exist')],404);
      } 
      #check if the requesting user is the admin of the main side which is side or hosting side . 
      if(!$this->checkIsGameSideAdmin($request->match_id,0,$user->id)){
         return response()->json(['message'=>__('match.not_game_owner')],403);
      }
      #check if there is user account provided and if so then check if the user owns the accout or not . 
      if($request->has('user_account_id')){
         if(!$this->userHasGame($user->id,$match->game_id,$request->user_account_id)){
            return response()->json(['message'=>__('match.no_related_account')],403);
         }
      }
      #checking if the user already has the value with him or hersel or not . 
      if($request->has('price')){
          if(($user->wallet + $match->price) < $this->changeCurrency($request->price,$request->currency,$user->currency)){
            return response()->json(['message'=>__('match.not_enough_credit')],403);
            }
      }
      #if the status of the match is in a no return position then go into lock position
      if(in_array($match->status,['ongoing','finished','stopped','failed'])){
         return response()->json(['message'=>__('match.match_no_return_state')],403);
      }
      #edit the match
      $data = $request->all();
      $match->update($data);
    }
    
    public function cancellMatch(Request $request){
      if(!$match = $this->checkMatchExists($request->match_id,true)){
         return response()->json(['message'=>__('match.match_not_exist')],404);
      }
      #check if the requesting user is the admin of the main side which is  hosting side . 
      if(!$this->checkIsGameSideAdmin($request->match_id,0,$request->user()->id)){
         return response()->json(['message'=>__('match.not_game_owner')],403);
      }
      #if the status of the match is in a no return position then go into lock position
      if(in_array($match->status,['ongoing','finished','stopped','failed'])){
         return response()->json(['message'=>__('match.match_no_return_state')],403);
      }
      #get the corresponding side of the user in the match
      $userSide = $this->getSide($request->user()->id,$match->id);
      #time to delete the whole   match (this process is very importatn and should be done by absolute care)
      $result = $this->matches->deleteMatch($match->id,$userSide);
      if($result){#if the deletion process was successfull then return success messag
         return response()->json(['message'=>__('match.match_delete_success')],200);
      }else{
         return response()->json(['message'=>__('match.problem_deleting_data')],200);
      }
    }    


    #leave the match by the user  .
    public function userLeaveMatch(Request $request){
      if(!$match = $this->checkMatchExists($request->match_id,true)){
         return response()->json(['message'=>__('match.match_not_exist')],404);
      }
        #check if the requesting user is the admin of the main side which is side or hosting side .
      if($this->checkIsGameSideAdmin($request->match_id,0,$request->user()->id) || $this->checkIsGameSideAdmin($request->match_id,1,$request->user()->id)){
         #if he is main admin(creator of the game) then use cancel match game so the whole match will be cancelled
         return $this->cancellMatch($request);
      }
      if(!$this->userHasMatch($request->match_id,null,$request->user())){
         return response()->json(['message'=>__('match.user_not_have_match')],403);
      }
      #if the status of the match is in a no return position then go into lock position
      if(in_array($match->status,['ongoing','finished','stopped','failed'])){
         return response()->json(['message'=>__('match.match_no_return_state')],403);
      }
      $result = $this->matches->deleteUserFromMatch($request->match_id,null,$request->user());
      if($result)
      return response()->json(['message'=>__('match.user_leave_success')],200);
      else
      return response()->json(['message'=>__('match.match_delete_problem')],500);
    }
    #user invitations
    public function userInvitations(Request $request){
      return response()->json($request->user);
    }
    #get user invitations 
    public function getUserInvitations(Request $request){
      $invitations = MatchInvitation::where('target_user_id',$request->user()->id)->where('status','pending')->get();
      return response()->json($invitations);
    }
    #invite user to the match 
    public function inviteUserToMatch(Request $request){#the inviting user should be the game side admin or he cannot invite anyone 
      $user = $request->user();
      if($request->target_user_id == $user->id){#if the user is inviting himself 
         return response()->json(['message'=>__('match.cant_invite_himself')],401);
      }
      #check if the requesting user is the admin of the main side which is side or hosting side . 
      if(!$this->checkIsGameSideAdmin($request->match_id,null,$request->user()->id)){
         #if he is main admin(creator of the game) then use cancell match game so the whole match will be cancelled
         return response()->json(['message'=>__('match.not_side_admin')],401);
      }
      $side = $this->getSide($user->id , $request->match_id);
      #send the invitation to the user 
      $result = $this->matches->inviteUser($user->id,$request->target_user_id,$request->match_id,$side);
      if($result){
         return response()->json(['message'=>__('match.user_invite_success'),'result'=>$result],200);
      }else{
         return response()->json(['message'=>__('match.problem_inviting_user')],500);
      }
    }

    #this function will let the user join the game as a first person so that he will be the game admin 
    public function joinMatchAsSideAdmin(JoinMatchRequest $request){
      #if the user has not the game for himself
      if(!$this->userHasGame($request->user()->id ,$this->getGameFromMatch($request->match_id),$request->userAccountId)){
         return response()->json(['message'=>__('match.no_related_account')]);
      }
      #check if the matche exists
      if(!$this->checkMatchExists($request->match_id)){
         return response()->json(['message'=>__('match.match_not_exist')],404);
      }
      #checking if the game is filled or not (passing true will return boolean for the result)
      if(!$this->checkSideEmpty($request->match_id ,$request->side)){
         return response()->json(['message'=>__('match.another_team_on_side')],403);
      }
      #check if the user has enough credit 
      if(!$this->userHasCreditForMatch($request->user(),$request->match_id,0)){#cause the side is always the guest side then it will always be the guest side 
         return response()->json(['message'=>__('match.not_enough_credit')],403);
      }
      #if the user is already in the match
      if($this->checkUserAlreadyInMatch($request->user(),$request->match_id)){
         return response()->json(['message'=>__('match.already_in_match')],403);
      }

      
      $match_type = $this->userMatchCreditType($request->user()->id,$request->match_id,$request->side);

      #join the user to the match
      if(!$this->payForMatch($request->user()->id,$request->match_id,$match_type)){
         return response()->json(['message'=>__('match.problem_payment_for_user')],500);
      }

      $join_match_result = $this->matches->joinUserToMatches($request->user()->id,$request->match_id,$request->side,$request->userAccountId,true);
      $match = Match::find($request->match_id);
      if($join_match_result){
         $match->update([
            'guest_user_id'=>$request->user()->id
         ]);
         return response()->json(['message'=>__('match.side_admin_join')],200);
      }
      return  response()->json(['message'=>__('match.problem_joining_user')],500);   
    }
    #user will join the match by the invitation provided for him
    public function joinMatchByInvitations(Request $request){
      #find the invitation 
      if(!$invitation = $this->userHasInvitation($request->user()->id,$request->invitation_id)){
         return response()->json(['message'=>__('match.has_not_invitation')],403);
      }
      #if the user has not the game for himself
      if(!$this->userHasGame($request->user()->id ,$this->getGameFromMatch($invitation->match_id))){
         return response()->json(['message'=>__('match.no_related_account')]);
      }
      #check if the matche exists
      if(!$match=$this->checkMatchExists($invitation->match_id,true)){
         return response()->json(['message'=>__('match.match_not_exist')],404);
      }
      #if the the account provided in the request is not for the match specified game then return an error
      if($this->getGameByAccount($request->user()->id,$request->userAccountId) != $match->game_id){
         return response()->json(['message'=>__('match.no_related_account')],403);
      }
      #checking if the game is filled or not (passing true will return boolean for the result)
      if(!$this->checkMatchCapacity($request->match_id ,$invitation->side,true)){
         return response()->json(['message'=>__('match.match_capacity_full')],403);
      }
      #if the user is already in the match
      if($this->checkUserAlreadyInMatch($request->user(),$request->match_id)){
         return response()->json(['message'=>__('match.already_in_match')],403);
      }
      #check if the user has enough credit 
      if(!$this->userHasCreditForMatch($request->user(),$request->match_id,$invitation->side)){
         return response()->json(['message'=>__('match.not_enough_credit')],403);
      }
         #reduce the credit of the user
      $game_id = $this->getGameFromMatch($invitation->match_id);
      #getting the match_type = 
      $match_type = $this->userMatchCreditType($request->user()->id,$invitation->match_id,$invitation->match_side);
      #join the user to the match(add it to transaction so that if anything happens revert the whole process)
      $result = DB::transaction(function() use ($request,$invitation,$game_id,$match_type){
         if(!$this->payForMatch($request->user()->id,$invitation->match_id,$match_type)){
            return response()->json(['message'=>__('match.problem_payment_for_user')],500);
         }
          #join the user to match using the repo function
         $join_match_result = $this->matches->joinUserToMatches($request->user()->id,$request->match_id,$invitation->match_side,$request->userAccountId,true);
         if($join_match_result){
            return $invitation->update([
               'status'=>'accepted'
            ]);
         }
            return  false;
      });
      return $result? response()->json(['message'=>__('match.user_join_success')],200):response()->json(['message'=>'there was a problem joining the user to the game'],500);
    }
    #reject the invitation sent by another user1
    public function rejectMatchInvitation(Request $reqeust){
        if(! $invitation = $this->userHasInvitation($reqeust->user()->id,$reqeust->invitation_id)){
            return response()->json(['message'=>__('match.has_not_invitation')]);
        }
        if(in_array($invitation->status,['accepted','rejected','cancelled'])){
            return response()->json(['message'=>'gameAdmin.invitation_not_valid_anymore']);
        }
        $invitation->update([
            'status'=>'rejected'
        ]);
    }
    #kick the match of the user if the user user is on the same side that the intended user for kicking is 
    public function kickMatchUser(Request $request){
         $user = $request->user();
         if($user->id == $request->user_id){
            return response()->json(['message'=>__('match.cant_kick_himself')],403);
         }
         if(!$match = $this->checkMatchExists($request->match_id,true)){
            return response()->json(['message'=>'match does not exist'],404);
         }
         #check if the requesting user is the admin of the main side which is side or hosting side .
         if(!$this->checkIsGameSideAdmin($request->match_id,null,$user->id)){
            #if he is main admin(creator of the game) then use cancell match game so the whole match will be cancelled
            return response()->json(['message'=>__('match.not_side_admin')],403);
         }
         $admin_side = $this->getSide($request->user_id, $request->match_id);

         $user_side = $this->getSide($request->user_id, $request->match_id);

         if($admin_side != $user_side)
             return response()->json(['message'=>__('match.cant_kick_other_side_users')]);
         #if the status of the match is in a no return position then go into lock position
         if(in_array($match->status,['ongoing','finished','stopped','failed'])){
            return response()->json(['message'=>__('match.match_no_return_state')],403);
         }
         $result = $this->matches->deleteUserFromMatch($request->match_id,$request->user_id);
         if($result)
            return response()->json(['message'=>__('match.user_leave_success')],200);
         else
            return response()->json(['message'=>__('match.problem_deleting_user_match')],500);
    }
    #kick the whole side of the match if the user is main admin of the game 
    public function kickMatchSide(Request $request){   
         $user = $request->user();
         $match = Match::find($request->match_id);
         #check if the requesting user is the admin of the main side which is side or hosting side . 
         if(!$this->checkIsGameSideAdmin($request->match_id,0,$user->id)){
            #if he is main admin(creator of the game) then use cancell match game so the whole match will be cancelled
            return response()->json(['message'=>__('match.not_game_owner')],403);
         }
         #if the status of the match is in a no return position then go into lock position
         if(in_array($match->status,['ongoing','finished','stopped','failed'])){
            return response()->json(['message'=>__('match.match_no_return_state')],403);
         }
         #if all conditions are set : 
         $this->matches->kickMatchSide($request->match_id,1);#the only side that the admin can kick is the main game admin and the match owner
    }
   
    #this function is still very arguable 
    public function kickAdmin(Request $request){
            
    } 
}
