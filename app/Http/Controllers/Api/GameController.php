<?php

namespace App\Http\Controllers\Api;

use App\Models\Game;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Interfaces\GameControllerInterface;

class GameController extends Controller implements GameControllerInterface
{
    public function index(Request $request){
        $games = '';
        if(!is_null($request->category_id)){
            $category = Category::find($request->category_id);
            return $category->games;
        }
        else{
            $game_obj = new Game;
            return $game_obj->all();
        }
    }
}
