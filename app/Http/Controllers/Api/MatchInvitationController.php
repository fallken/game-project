<?php

namespace App\Http\Controllers\Api;

use App\Traits\Utils;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Interfaces\MatchesInterface;
use App\Http\Interfaces\MatchInvitationInterface;

class MatchInvitationController extends Controller 
{
    use Utils;
    public $matches ,$invitationRepo;


    public function __construct(MatchesInterface $matches)
    {
        $this->matches = $matches;
    }
    #send match invitation to specific user .
    public function sendMatchInvitation(Request $request){
        #check if is match admin
        if(!$this->checkIsGameSideAdmin($request->match_id,$request->user()->id,$request->side)){
            return response()->json(['message'=>'user is  not  match admin so he cannot send invitation'],403);
        }
        #check if match is fnished or not 
        $match_status = $this->matches->matchCurrentStatus($request->match_id);
        if( $match_status!== config('matches.status.0') || $match_status !== config('matches.status.1')){
            return response()->json(['message'=>'you can only invite a player when the match is on pending status'],403);
        }
        #check if match has enough save
        if(!$this->checkMatchCapacity($request->match_id,$request->side,true)){
            return response()->json(['message'=>'the match does  not have enough capacity for the current users'],403);
        }
        #send the invitation successfully 
        $result = $this->matches->sendInvitation($request->user()->id,$request->target_user_id,$request->match_id,$request->side);
        #return the result of invitation wether it was a failure or success
        return $result;
    }
    #cancel the invitation user has send to anothe user
    public function cancelMatchInvitation(Request $reqeust){
        #check if the invitation exists
        if(! $invitation = $this->userOwnInvitation($reqeust->user()->id,$reqeust->invitation_id)){
             return response()->json(['message'=>'invitation deleted or user has no invitation']);
        }
        #check if the invitation is closed or not
        if(in_array($invitation->status,['accepted','rejected','cancelled'])){
            return response()->json(['message'=>'invitation is already accepted rejected or cancelled ']);
        }

        $result = $invitation->update([
            'status'=>'cancelled'
        ]);

        return $result;
    }
    #reject the invitation sent by another user1
    public function rejectMatchInvitation(Request $reqeust){
        if(! $invitation = $this->userHasInvitation($reqeust->user()->id,$reqeust->invitation_id)){
            return response()->json(['message'=>'the invitation is not for the user ']);
        }
        if(in_array($invitation->status,['accepted','rejected','cancelled'])){
            return response()->json(['message'=>'invitation is already accepted rejected or cancelled ']);
        }
        $invitation->update([
            'status'=>'rejected'
        ]);
    }
    #get the list of match invitations for the logged in user 
    public function getMatchInvitations(Request $reqeust){
        #return the list of user invitations by filter
        $this->matches->getInvitations($reqeust->user()->id,true);
    }


}
