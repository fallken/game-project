<?php

namespace App\Http\Requests\API;

use Illuminate\Foundation\Http\FormRequest;

class CreateMatchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'game_id'=>'required|numeric',
            'price'=>'numeric',
            'max_players'=>'numeric',
            'message'=>'string',
            'min_level_allowed'=>'numeric',
            'min_level_allowed'=>'numeric',
        ];
    }
}
