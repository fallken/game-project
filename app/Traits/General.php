<?php

namespace App\Traits;

use App\Models\User;
use Illuminate\Support\Facades\Redis;

trait General{
     /**
     * @param $token
     * @return array
     * desc: will return the cookie details for the authenticated user 
     */
    private function getCookieDetails($token)
    {
        return [
            'name' => '_token',
            'value' => $token,
            'minutes' => 1440,
            'path' => null,
            'domain' => null,
            // 'secure' => true, // for production
            'secure' => null, // for localhost
            'httponly' => true,
            'samesite' => true,
        ];
    }

    public function getRelations($relationString)
    {
        if(!$relationString)return null;
        if(strpos($relationString , ',')):
            $relations = explode(',',$relationString);
            else://if there will be only one relation passed by the user . 
            $relations = $relationString;
        endif;
        return $relations;
    }

    public function pushEventIntoRedis(string $name,array $data)
    {
        $result = Redis::publish('messages', json_encode(['foo' => 'bar','some info'=>'asdfasdf']));
        return $result;
    }
}