<?php


namespace App\Traits;

use App\Models\Game;
use App\Models\User;
use App\Models\Match;
use App\Models\Message;
use App\Models\UserGame;
use App\Models\GameAdmin;
use App\Models\UserFriend;
use App\Models\UserMatch;
use Illuminate\Support\Arr;
use App\Models\MatchInvitation;
use App\Models\UserTransaction;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

trait Utils{
    public function checkIsGameSideAdmin($match_id,$match_side=null,$user_id){
        
        #getting the match side if its set null
        $match_side=!is_null($match_side)?$match_side:$this->getSide($user_id,$match_id)??null;
        $match = Match::findOrFail($match_id);

        if(is_null($match_side)){
            return false;
        }
        if($match_side == 0){#get the side they want 
            if($match->host_user_id != $user_id){#if the user is not from the hosting side 
                return false;
            }
        }else if($match_side ==  1){#check if the user is guest side admin
            if($match->guest_user_id !== $user_id){#if the user is not the admin of the guest side 
                return false;
            }
        }
        return true;
    }
    
    public function userHasAnyMatch($user_id=null,$user=null){
        $customer = !is_null($user)?$user:User::findOrFail($user_id);

        if(!$customer->matches)return false;

        return true;
    }
    public function getFriendIds($user_id){
        $friends = UserFriend::where('user_id',$user_id)->where('status','accepted')->pluck('friend_id');
        return $friends;
    }
    public function userHasMatch($match_id,$user_id=null,$user=null){
        $user = !is_null($user)?$user:User::findOrFail($user_id);
        if(!$this->userHasAnyMatch($user->id))return false;
        $user_matches = Arr::pluck($user->matches,['id']);
        return in_array($match_id,$user_matches)?true:false;
    }

    public function getSide($user_id , $match_id){

        #get the user matches 
        $user_match= DB::table('user_matches')->where('user_id',$user_id)->where('match_id',$match_id)->first();
        return $user_match?$user_match->side:null;
    }
    #for multiple games
    public function userHasGames($user_id){
        $usersGames= DB::table('user_games')->where('user_id',$user_id)->count();
        if($usersGames == 0){
            return false;
        }
        return true;
    }
    #for one game
    public function userHasGame($user_id , $game_id , $account_id=null){
        // exit(json_encode([$user_id,$game_id,$account_id]));
        $userGame= UserGame::where('user_id',$user_id)->where('game_id',$game_id);
        if($account_id)$userGame = $userGame->where('id',$account_id);
        $userGame = $userGame->count();
        return $userGame == 0 ? false : true;
    }

    #check if the user has an invitation like this sent to him
    public function userHasInvitation($user_id , $invitation_id){
        $invitation = MatchInvitation::findOrFail($invitation_id);

        if($invitation->target_user_id != $user_id)return false;

        return $invitation;
    }
    #return the invitations of the current user 
   
    #check if the user is the owner of the invitation
    public function userOwnInvitation($user_id , $invitation_id){
        $invitation = MatchInvitation::findOrFail($invitation_id);

        if($invitation_id->host_user_id != $user_id)return false;

        return $invitation;
    }
    public function getUserMatchRow($user_id , $match_id){
        $user_match_row = UserMatch::where('user_id',$user_id)->where('match_id',$match_id)->first();
        return $user_match_row;
    }

    public function getGameFromMatch($match_id,$returnGame=false){
        $match = Match::findOrFail($match_id);
        if($returnGame){
            return Game::findOrFail($match->game_id);
        }else{
            return $match->game_id;
        }
    }

    public function checkMatchExists($match_id,$getMatchObj=false){
        $match = Match::find($match_id);
        if(!$match)return false;
        return $getMatchObj?$match:true;
    }


    public function getGameByAccount($user_id,$user_account_id){
       $result = UserGame::find($user_account_id);
       return $result->game_id;
    }

    #checking if the match already has capacity or not
    public function checkMatchCapacity($match_id , $side ,$checkBool=false){
        $match = Match::findOrFail($match_id);
        $side = config('matches.sides.'.$side);
        $side_capacity=0;
        if(is_null($match->$side)){
            $side_capacity = $match->max_players;
        }else{
            $side_capacity = $match->$side;
        }

        $match_side_users = DB::table('user_matches')
        ->where('match_id',$match_id)->where('side',$side)->count();

        #if the number of joined users is equal to number of users in the matches side then its as follow 
        if($side_capacity == $match_side_users){
            return $checkBool?false:0;
        }

        return $checkBool?true:($side_capacity-$match_side_users);
    }
    #check what sort of payment will the user have to pay for the game 
    public function userMatchCreditType($user_id,$match_id,$side,$match_object=null){
        $match = $match_object??Match::findOrFail($match_id);
        if($match->price <= 0)return config('matches.matchPayment.free');
        if($side == 0){
            if($match->host_pay == 'all'){
                return config('matches.matchPayment.all');
            }else{
                return config('matches.matchPayment.single');
            }
        }else{
            if($match->guest_pay == 'all'){
                return config('matches.matchPayment.all');
            }else{
                return config('matches.matchPayment.single');
            }
        }
    }

    #check if user has credit enought for the match or not . 
    public function userHasCreditForMatch($user,$match_id,$side){
        $match = Match::findOrFail($match_id);
        $payment_type = $this->userMatchCreditType($user->id,$match_id,$side,$match);
        if($payment_type == 2 || $payment_type == 1){#if the the game is free or its already paid then return true
            return true;
        }else{
            if($user->wallet < $match->price){#if user already has not enough money with him
                return false;
            }else{
                return true;
            }
        }
    }       
    #check if  the requesting game exists or not 
    public function checkGameExists($game_id){
        $game = Game::find($game_id);
        return $game??false;    
    }
    #check if the user is already in the match
    public function checkUserAlreadyInMatch($user,$match_id,$side=null){
        $user_matches_count=DB::table('user_matches')->where('user_id',$user->id)->where('match_id',$match_id);

        if($side){#if the  entity is adding side too 
            $user_matches_count->where('side',$side);
        }
        $result = $user_matches_count->count();
        return $result > 0?true:false;
    }

    public function payForMatch($user_id,$match_id,$match_type){
        if($match_type == 1 || $match_type==2){//if the type of the match is free or is paid by the owner of the group 
            return true;//no need to reduce from user's credit 
        }
        $match = Match::findOrFail($match_id);

        $user=User::findOrFail($user_id);
        return DB::transaction(function() use($user,$match,$match_id) {
            $user->update([
            'wallet' => $user->wallet - $match->price
            ]);
            Log::info('working like charm');
            $data=[
                'user_id'=>$user->id,
                'game_id'=>$match->game_id,
                'match_id'=>$match_id,
                'amount'=>$match->price,
                'type'=>config('matches.transactionTypes.6'),
                'desc'=>"user payment for match $match_id"
            ];
            return UserTransaction::create($data);
        });
    }
    #getting the current status of the match
    public function getMatchStatus($match_id){
        $match = Match::findOrFail($match_id);
        return $match->status;
    }
    #checking if the match already has a game admin or not 
    public function matchHasAdmin($match_id){
        $match = Match::findOrFail($match_id);
        #return true if admin id in not null nad vice versa
        return $match->admin_id??false;
    }
    #check if the user is admin himself or not
    public function checkIsAdmin($user_id,$admin_id=null){
        $admin = GameAdmin::where('user_id',$user_id)->where('status','active');
        if($admin_id)$admin->where('id',$admin_id);
        $admin = $admin->first();
        return $admin?$admin:false;
    }
    #check is admin of the game 
    public function isGameAdmin($user_id, $game_id){
        $result = GameAdmin::where('user_id',$user_id)->where('game_id',$game_id)->where('status','!=','rejected')->count();
        if($result > 0){
            return true;
        }
        return false;
    }
    #check if the game admin is actually the admin of the match
    public function checkIsMatchGameAdmin($match_id,$admin_id=null){
        $match = Match::find($match_id);

        if($match->admin_id != $admin_id){
            return false;
        }
        return true;
    }
    #will return the users inside each side of the match
    #will return array of objects if onthing is passed to it 
    public function getMatchUsersBySide($match_id,$side=null){
        $user = new User;
        $query = DB::table('user_matches')->select(['user_id'])->where('match_id',$match_id);
        if(is_null($side)){
            $host_side = DB::table('user_matches')->select(['user_id'])->where('match_id',$match_id)->where('side',0)->pluck('user_id');
            $guest_side =DB::table('user_matches')->select(['user_id'])->where('match_id',$match_id)->where('side',1)->pluck('user_id');
            $host_users = $user->whereIn('id',$host_side)->get();
            $guest_users = $user->whereIn('id',$guest_side)->get();
            return [
                'host_users'=>$host_users,'guest_users'=>$guest_users
            ];
        }
        $side_users = $query->where('side',$side)->pluck('user_id');

        $side_users = $user->whereIn('id',$side_users)->get();

        return $side_users;
    }
    #convert side from boolean or integer into string . 
    public function getSideString($side){
        return $side == 0 ? 'host':'guest';
    }
    #a function for changing the currency of the user 
    public function changeCurrency($input_money,$currency_from=null,$currency_to=null){
        return $input_money;
    }
    #check if there is already any account there or not 
    public function accountExists($account_id,$game_id){
        $whereClause = 'account_user_name';
        $account = UserGame::where('game_id',$game_id)->Where($whereClause,$account_id)->first();
        if($account)return $account;
        else return false;
    }
    #get game account information using the api provided for the project .
    public function getAccountFromApi($account_id,$game_id){
        $api = '';
        #list the endpoint here 
        switch($game_id){
            case 1:
                $api = 'localhost:4884/clashOfClans';
            break;
            case 2:#if the game is counter then do nothing and return empty string
                return '';
                // $api = 'localhost:4883/counterCsGo';
            break; 
            case 3:#if call of duty
                return '';
            break; 
            case 4:
                $api = 'localhost:4881/dota2';
            break;
            #request the user information from the microservices  


            return '';
        }
    }
    #will return the owner of the specific admin
    public function getUserFromAdminId($admin_id,$withAdmin=false){
        $admin = GameAdmin::find($admin_id);
        if($admin){#if there is an admin with that name then return the user for this admin
            if($withAdmin){
                return ['user'=>$admin->user,'admin'=>$admin];
            }
            return $admin->user;
        }
    }
    #check if specific side of the match is empty or not . 
    public function checkSideEmpty($match_id,$side){
        $resultCount = DB::table('user_matches')->where('side',$side)->where('match_id',$match_id)->count();
        #if there are actually users like this in one side . 
        if($resultCount > 0)return false;
        return true;
    }
    #this funtion checks if the user is friend with another user or not . 
    public function userIsFriend($user_id,$friend_id){
        #im making two requests . i wonder if its a good choice or not (im doing this so that im not instrting two similar users in the field );
        $owner = UserFriend::where('user_id',$user_id)->where('friend_id',$friend_id)->where('status','accepted')->count();
        $friend = UserFriend::where('user_id',$friend_id)->where('friend_id',$friend_id)->where('status','accepted')->count();

        if($owner > 0 || $friend >0) return true;

        return false;
    }
    #friendRequestAlreadyExists . check if the friend request already exists 
    public function friendRequestAlreadyExists($user_id,$friend_id){
       
       $friend_req_count = UserFriend::where('user_id',$user_id)->where('friend_id',$friend_id)->where('status','pending')->count();
       if($friend_req_count ==  0 ){
            return false;
       }

       return true;
    }

    public function getFriendRequests($user_id){
        $results = UserFriend::where('friend_id',$user_id)->where('status','pending')->with('requestOwner')->get();
        return $results;
    }
}