<?php
/**
 * Created by PhpStorm.
 * User: NilGooN
 * Date: 20/11/2019
 * Time: 05:24 PM
 */

namespace App\Traits;


use App\Events\SocketMessage;
use App\Models\Message;
use Predis\Collection\Iterator\SortedSetKey;

trait Messaging
{
    public function updateMessage(array $data)
    {

    }
    public function deleteMessage($message_id)
    {

    }
    public function getMessage($message_id)
    {

    }
    public function sendMessage(array $data,$notify=true)
    {
        //the input data should be valid other wise the message wont be created
        $result = Message::create($data);
        if($notify){
            $this->messageEventDispatchEngin($result);
        }

        return $result;
    }

    /*
     * desc:will check the type of message and then will identify the type and in the final step will create the message socket event by the data passed to it .
     */
    public function messageEventDispatchEngin($message){
        $socket_data = [
            'from'=>null,//could be null.
            'name'=>'talk',
            'message'=>'some dummy message',
            'details'=>[],
            'all'=>false,
            'to'=>null
        ];
        switch ($message->type){
            case 'system':
                $socket_data['all'] = true;
                $socket_data['name'] = 'system';
                $socket_data['message'] = [
                    'message'=>$message->text
                    //we can add other options as well
                ];
            break;
            case 'talk':
                $socket_data['all'] = false;
                $socket_data['name'] = 'talk';
                $socket_data['message'] = $message->text;
                $socket_data['to']=$message->target_user_id;
                $socket_data['from']=$message->user_id;
            break;
            //the other type of pushes will be handled by the code for now.
        }

        event(new SocketMessage('message',$socket_data));
    }


}