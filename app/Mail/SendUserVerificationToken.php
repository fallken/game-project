<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendUserVerificationToken extends Mailable
{
    use Queueable, SerializesModels;
    public $userEmail,$token,$subject;

    /**
     * SendUserVerificationToken constructor.
     * @param $userEmail
     * @param $token
     * @param string $subject
     */
    public function __construct($userEmail,$token,$subject='Sign up activation')
    {
        $this->userEmail = $userEmail;
        $this->token = $token;
        $this->subject = $subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.sign_up_verification')
            ->with(['token'=>$this->token,'userEmail'=>$this->userEmail])
            ->from('info@gamecourt.com')
            ->subject($this->subject);
    }
}
