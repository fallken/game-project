<?php

namespace App\Interfaces;

use Illuminate\Http\UploadedFile;

interface GameFilesInterface
{
    /**
     * Fetch game file from data storage.
     *
     * @param $id
     *
     * @return \App\Models\GameFile|null
     */
    public function fetchById($id);

    /**
     * Upload and save file info to data storage
     *
     * @param UploadedFile $file
     * @param string $type
     *
     * @return mixed
     */
    public function upload(UploadedFile $file, string $type);

    /**
     * Upload many files.
     *
     * @param array $files
     * @param string $type
     *
     * @return array
     */
    public function uploadMany(array $files, string $type): array;

    /**
     * Remove game file form data storage.
     *
     * @param object $gameFile
     *
     * @return bool
     */
    public function remove($gameFile): bool;

    /**
     * Get file direct link.
     *
     * @param object $gameFile
     *
     * @return string
     */
    public function asset($gameFile);
}
