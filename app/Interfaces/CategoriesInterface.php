<?php

namespace App\Interfaces;

use App\Models\Category;
use App\Repositories\CategoriesRepository;

interface CategoriesInterface
{
    /**
     * Fetch category from data storage by id.
     *
     * @param $id
     *
     * @return mixed
     */
    public function fetchById($id);

    /**
     * Fetch categories by name.
     *
     * @param string $name
     * @param string $sortType
     * @param int $limit
     *
     * @return mixed
     */
    public function fetchByName(string $name, string $sortType = 'asc', int $limit = 100);

    /**
     * Set category name.
     *
     * @param string $name
     * @return mixed
     */
    public function setName(string $name): CategoriesRepository;

    /**
     * Set category image.
     *
     * @param string $imageName
     * @param mixed $id
     *
     * @return mixed
     */
    public function setImage(string $imageName, $id = null): CategoriesRepository;

    /**
     * Save category to data storage.
     *
     * @param null $id
     *
     * @return Category
     */
    public function save($id = null): Category;

    /**
     * Remove category from data storage.
     *
     * @param $id
     * @param bool $forceDeletes
     * @return mixed
     */
    public function remove($id, bool $forceDeletes = false);

    /**
     * Store or updated category.
     *
     * @param string $name
     * @param \Illuminate\Http\UploadedFile|null $image
     * @param int|null $id
     *
     * @return mixed
     */
    public function storeOrUpdate(string $name, $image = null, $id = null);
}
