CREATE USER 'matchers'@'%' IDENTIFIED BY 'matchers';
GRANT ALL PRIVILEGES ON * . * TO 'matchers'@'%';

CREATE DATABASE matchers;

FLUSH PRIVILEGES;