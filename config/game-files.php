<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Domain
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default domain that should be used
    | by the game files repository.
    */

    'path' => env('GAME_FILE_PATH', 'files/'),

    'domain' => env('GAME_FILE_DOMAIN', 'http://matchers.local/'),
];
